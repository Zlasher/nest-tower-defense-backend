import { Message } from 'discord.js';

export class MessageToUpperPipe {
  transform([message]: [Message]): [Message] {
    message.content = message.content.toUpperCase();

    return [message];
  }
}
