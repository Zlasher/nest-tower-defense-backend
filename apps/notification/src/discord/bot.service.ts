import { Client, Webhook } from 'discord.js';

import { CreateNotificationDTO } from '@app/api/notification-api';
import { InjectDiscordClient, On } from '@discord-nestjs/core';
import { Injectable, Logger } from '@nestjs/common';

import { WehhookConfig } from './types';
import { appVersion } from '@app/common';

@Injectable()
export abstract class BotService {
  protected webHook: Webhook;
  protected readonly logger = new Logger(BotService.name);

  constructor(
    @InjectDiscordClient()
    protected readonly client: Client,
  ) {}

  getWebhookConfig(): WehhookConfig {
    throw 'Cannot use config from base';
  }

  async fetchWebhook(): Promise<void> {
    if (this.webHook) {
      return;
    }

    try {
      const { channelId, token } = this.getWebhookConfig();

      this.webHook = await this.client.fetchWebhook(channelId, token);
      this.onWebhookConnected();
    } catch (e) {
      this.logger.log('Bot webhook error:', e);
    }
  }

  onWebhookConnected(): string {
    const message = `🚀 ${this.getWebhookConfig().name} ${
      this.client?.user?.username
    } (v${appVersion}) up and running...`;

    this.logger.log(message);
    return message;
  }

  handleCreateNotification(payload: CreateNotificationDTO) {
    if (!this.webHook) {
      this.logger.error('Webhook not ready!');
      return;
    }

    if (!payload.message) {
      this.logger.error('Notification message was empty');
      return;
    }

    try {
      this.webHook.send(payload.message);
    } catch (e) {
      this.logger.error('Error creating message', e);
    }
  }

  @On('ready')
  onReady() {
    console.log('On ready');
    this.fetchWebhook();
  }

  @On('error')
  onError(error: any) {
    this.logger.log(`Bot error!`, error);
  }
}
