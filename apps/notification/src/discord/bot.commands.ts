import { Module } from '@nestjs/common';

import { DiscordModule } from '@discord-nestjs/core';
import { GetInfoCommand } from './commands/get-player.command';

@Module({
  imports: [DiscordModule.forFeature()],
  providers: [GetInfoCommand],
})
export class BotCommands {}
