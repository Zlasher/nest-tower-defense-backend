import { Message } from 'discord.js';

export class MessageFromUserGuard {
  canActive(event: 'messageCreate', [message]: [Message]): boolean {
    console.log('message', message.author);

    return !message.author.bot;
  }
}
