import { Client } from 'discord.js';

import { InjectDiscordClient } from '@discord-nestjs/core';
import { Injectable } from '@nestjs/common';
import { BotService } from '../bot.service';
import { WehhookConfig } from '../types';

@Injectable()
export class PurchasesHookService extends BotService {
  constructor(
    @InjectDiscordClient()
    protected readonly client: Client,
  ) {
    super(client);
  }

  getWebhookConfig(): WehhookConfig {
    return {
      name: 'Purchases Webhook',
      channelId: process.env.DISCORD_WEBHOOK_PURCHASES_CHANNEL_ID,
      token: process.env.DISCORD_WEBHOOK_PURCHASES_TOKEN,
    };
  }
}
