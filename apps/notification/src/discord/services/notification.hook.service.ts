import { Client } from 'discord.js';

import { InjectDiscordClient } from '@discord-nestjs/core';
import { Injectable } from '@nestjs/common';
import { BotService } from '../bot.service';
import { WehhookConfig } from '../types';

@Injectable()
export class NotificationHookService extends BotService {
  constructor(
    @InjectDiscordClient()
    protected readonly client: Client,
  ) {
    super(client);
  }

  getWebhookConfig(): WehhookConfig {
    return {
      name: 'Notification Webhook',
      channelId: process.env.DISCORD_WEBHOOK_INFO_CHANNEL_ID,
      token: process.env.DISCORD_WEBHOOK_INFO_TOKEN,
    };
  }

  onWebhookConnected(): string {
    const message = super.onWebhookConnected();

    this.webHook.send(message);
    return message;
  }
}
