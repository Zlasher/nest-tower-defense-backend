import { Client } from 'discord.js';

import { InjectDiscordClient } from '@discord-nestjs/core';
import { Injectable } from '@nestjs/common';
import { BotService } from '../bot.service';
import { WehhookConfig } from '../types';

@Injectable()
export class ArenaHookService extends BotService {
  constructor(
    @InjectDiscordClient()
    protected readonly client: Client,
  ) {
    super(client);
  }

  getWebhookConfig(): WehhookConfig {
    return {
      name: 'Arena Webhook',
      channelId: process.env.DISCORD_WEBHOOK_CHAMPION_ARENA_CHANNEL_ID,
      token: process.env.DISCORD_WEBHOOK_CHAMPION_ARENA_TOKEN,
    };
  }
}
