import { SlashCommandPipe } from '@discord-nestjs/common';
import {
  Command,
  EventParams,
  Handler,
  InteractionEvent,
} from '@discord-nestjs/core';
import { Injectable } from '@nestjs/common';
import { ClientEvents } from 'discord.js';
import { GetPlayerDTO } from '../dto/get-player.dto';

@Command({
  name: 'get-player',
  description: 'player details',
})
@Injectable()
export class GetInfoCommand {
  @Handler()
  onGetInfo(
    @InteractionEvent(SlashCommandPipe) options: GetPlayerDTO,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @EventParams() _args: ClientEvents['interactionCreate'],
  ): string {
    // console.log('args', args);
    return `User was registered with name: ${options?.playerId}`;
  }
}
