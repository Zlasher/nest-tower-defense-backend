import { Param } from '@discord-nestjs/core';

export class GetPlayerDTO {
  @Param({
    name: 'playerId',
    description: 'Id of the Player',
    required: true,
  })
  playerId: string;
}
