import { RmqService } from '@app/common/modules';
import { NestFactory } from '@nestjs/core';

import { NotificationModule } from './notification.module';
import { queues } from '@app/api/notification-api';
import { appVersion } from '@app/common';

async function bootstrap() {
  const app = await NestFactory.create(NotificationModule);
  const rmqService = app.get<RmqService>(RmqService);

  app.connectMicroservice(rmqService.getOptions(queues.notifications));
  await app.startAllMicroservices();

  const port = process.env.PORT || 4000;
  await app.listen(port);

  console.log(
    `🚀 Notification-service (v${appVersion}) up and running on port`,
    port,
  );
}
bootstrap();
