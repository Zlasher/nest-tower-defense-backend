import { GatewayIntentBits } from 'discord.js';

import { RmqModule } from '@app/common/modules';
import { DiscordModule } from '@discord-nestjs/core';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { NotificationController } from './controllers/notification.controller';
import { BotCommands } from './discord/bot.commands';
import { ArenaHookService } from './discord/services/arena.hook.service';
import { FeedbackHookService } from './discord/services/feedback.hook.service';
import { NotificationHookService } from './discord/services/notification.hook.service';
import { PurchasesHookService } from './discord/services/purchases.hook.service';
import { CacheModule } from '@nestjs/cache-manager';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: './apps/notification/.env',
    }),
    RmqModule,
    DiscordModule.forRootAsync({
      useFactory: () => ({
        token: process.env.DISCORD_BOT_TOKEN,
        prefix: '%',
        discordClientOptions: {
          intents: [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.GuildMessages,
            GatewayIntentBits.MessageContent,
          ],
        },
        registerCommandOptions: [
          {
            forGuild: '785866018513879040',
            // removeCommandsBefore: true,
          },
        ],
      }),
    }),
    BotCommands,
    CacheModule.register(),
  ],
  controllers: [NotificationController],
  providers: [
    NotificationHookService,
    PurchasesHookService,
    FeedbackHookService,
    ArenaHookService,
  ],
})
export class NotificationModule {}
