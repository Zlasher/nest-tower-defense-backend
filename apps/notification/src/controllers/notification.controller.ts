import {
  ChannelType,
  CreateNotificationDTO,
  topics,
} from '@app/api/notification-api';
import { RmqService } from '@app/common/modules';
import { Controller, Logger } from '@nestjs/common';
import { Ctx, EventPattern, Payload, RmqContext } from '@nestjs/microservices';

import { BotService } from '../discord/bot.service';
import { ArenaHookService } from '../discord/services/arena.hook.service';
import { FeedbackHookService } from '../discord/services/feedback.hook.service';
import { NotificationHookService } from '../discord/services/notification.hook.service';
import { PurchasesHookService } from '../discord/services/purchases.hook.service';

@Controller('notification')
export class NotificationController {
  protected readonly logger = new Logger(NotificationController.name);

  constructor(
    private readonly notificationHookService: NotificationHookService,
    private readonly purchasesHookService: PurchasesHookService,
    private readonly feedbackHookService: FeedbackHookService,
    private readonly arenaHookService: ArenaHookService,
    private readonly rmgService: RmqService,
  ) {}

  getClient(payload: CreateNotificationDTO): BotService {
    switch (payload?.channelType) {
      case ChannelType.Notification:
        return this.notificationHookService;
      case ChannelType.Purchases:
        return this.purchasesHookService;
      case ChannelType.Feedback:
        return this.feedbackHookService;
      case ChannelType.Arena:
        return this.arenaHookService;

      default:
        return this.notificationHookService;
    }
  }

  @EventPattern(topics.createNotification)
  handleNotificationEvent(
    @Payload() payload: CreateNotificationDTO,
    @Ctx() context: RmqContext,
  ) {
    if (!payload) {
      this.logger.error('Notification event was empty');
      return;
    }

    this.getClient(payload).handleCreateNotification(payload);

    this.rmgService.ack(context);
  }

  // @On('messageCreate')
  // async onMessage(message: Message): Promise<void> {
  //   console.log('message', message.content);

  //   if (message.author.bot) {
  //     return;
  //   }

  //   await message.reply('Message processed successfully');
  // }
}
