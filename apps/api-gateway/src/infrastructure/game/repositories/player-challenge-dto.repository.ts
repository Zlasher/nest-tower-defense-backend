import { Model } from 'mongoose';

import { BaseDtoRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { PlayerChallengeSchema } from '../schemas/player-challenge.schema';

@Injectable()
export class PlayerChallengesDtoRepository extends BaseDtoRepository<PlayerChallengeSchema> {
  constructor(
    @InjectModel(PlayerChallengeSchema.name)
    schemaModel: Model<PlayerChallengeSchema>,
  ) {
    super(schemaModel);
  }
}
