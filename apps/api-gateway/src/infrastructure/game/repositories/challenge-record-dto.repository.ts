import { Model } from 'mongoose';

import { BaseDtoRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { ChallengeRecordSchema } from '../schemas/challenge-record.schema';

@Injectable()
export class ChallenngeRecordsDtoRepository extends BaseDtoRepository<ChallengeRecordSchema> {
  constructor(
    @InjectModel(ChallengeRecordSchema.name)
    schemaModel: Model<ChallengeRecordSchema>,
  ) {
    super(schemaModel);
  }
}
