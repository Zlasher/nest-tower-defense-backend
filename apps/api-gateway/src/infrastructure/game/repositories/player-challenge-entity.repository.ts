import { Model } from 'mongoose';

import { BaseEntityRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { PlayerChallenge } from '../../../domain/game';
import { PlayerChallengeSchema } from '../schemas/player-challenge.schema';
import { PlayerChallengeSchemaFactory } from '../factories/player-challenge-schema.factory';

@Injectable()
export class PlayerChallengeEntityRepository extends BaseEntityRepository<
  PlayerChallengeSchema,
  PlayerChallenge
> {
  constructor(
    @InjectModel(PlayerChallengeSchema.name)
    model: Model<PlayerChallengeSchema>,
    schemaFactory: PlayerChallengeSchemaFactory,
  ) {
    super(model, schemaFactory);
  }
}
