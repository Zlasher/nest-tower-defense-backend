import { Model } from 'mongoose';

import { BaseDtoRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { GameRoundSchema } from '../schemas/game-round.schema';

@Injectable()
export class GameRoundsDtoRepository extends BaseDtoRepository<GameRoundSchema> {
  constructor(
    @InjectModel(GameRoundSchema.name)
    schemaModel: Model<GameRoundSchema>,
  ) {
    super(schemaModel);
  }
}
