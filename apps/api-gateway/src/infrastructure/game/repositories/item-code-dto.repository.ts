import { Model } from 'mongoose';

import { BaseDtoRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { ItemCodeSchema } from '../schemas/item-code.schema';

@Injectable()
export class ItemCodesDtoRepository extends BaseDtoRepository<ItemCodeSchema> {
  constructor(
    @InjectModel(ItemCodeSchema.name)
    schemaModel: Model<ItemCodeSchema>,
  ) {
    super(schemaModel);
  }
}
