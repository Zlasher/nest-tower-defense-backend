import { Model } from 'mongoose';

import { BaseEntityRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { MapMessage } from '../../../domain/game';
import { MapMessageSchemaFactory } from '../factories/map-message-schema.factory';
import { MapMessageSchema } from '../schemas/map-message.schema';

@Injectable()
export class MapMessageEntityRepository extends BaseEntityRepository<
  MapMessageSchema,
  MapMessage
> {
  constructor(
    @InjectModel(MapMessageSchema.name)
    model: Model<MapMessageSchema>,
    schemaFactory: MapMessageSchemaFactory,
  ) {
    super(model, schemaFactory);
  }
}
