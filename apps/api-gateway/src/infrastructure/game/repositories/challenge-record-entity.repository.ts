import { Model } from 'mongoose';

import { BaseEntityRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { ChallengeRecord } from '../../../domain/game';
import { ChallengeRecordSchemaFactory } from '../factories/challenge-record-schema.factory';
import { ChallengeRecordSchema } from '../schemas/challenge-record.schema';

@Injectable()
export class ChallengeRecordEntityRepository extends BaseEntityRepository<
  ChallengeRecordSchema,
  ChallengeRecord
> {
  constructor(
    @InjectModel(ChallengeRecordSchema.name)
    model: Model<ChallengeRecordSchema>,
    schemaFactory: ChallengeRecordSchemaFactory,
  ) {
    super(model, schemaFactory);
  }
}
