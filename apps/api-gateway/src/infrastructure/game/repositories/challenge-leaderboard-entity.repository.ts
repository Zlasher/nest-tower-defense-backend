import { ChallengeLeaderboard } from 'apps/api-gateway/src/domain/game';
import { Model } from 'mongoose';

import { BaseEntityRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { ChallengeLeaderboardSchemaFactory } from '../factories/challenge-leaderboard-schema.factory';
import { ChallengeLeaderboardSchema } from '../schemas/challenge-leaderboard.schema';

@Injectable()
export class ChallengeLeaderboardEntityRepository extends BaseEntityRepository<
  ChallengeLeaderboardSchema,
  ChallengeLeaderboard
> {
  constructor(
    @InjectModel(ChallengeLeaderboardSchema.name)
    model: Model<ChallengeLeaderboardSchema>,
    schemaFactory: ChallengeLeaderboardSchemaFactory,
  ) {
    super(model, schemaFactory);
  }
}
