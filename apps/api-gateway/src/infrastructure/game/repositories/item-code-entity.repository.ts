import { Model } from 'mongoose';

import { BaseEntityRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { ItemCode } from '../../../domain/game';
import { ItemCodeSchemaFactory } from '../factories/item-code-schema.factory';
import { ItemCodeSchema } from '../schemas/item-code.schema';

@Injectable()
export class ItemCodeEntityRepository extends BaseEntityRepository<
  ItemCodeSchema,
  ItemCode
> {
  constructor(
    @InjectModel(ItemCodeSchema.name)
    model: Model<ItemCodeSchema>,
    schemaFactory: ItemCodeSchemaFactory,
  ) {
    super(model, schemaFactory);
  }
}
