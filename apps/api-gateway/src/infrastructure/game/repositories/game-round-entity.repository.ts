import { Model } from 'mongoose';

import { BaseEntityRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { GameRound } from '../../../domain/game';
import { GameRoundSchemaFactory } from '../factories/game-round-schema.factory';
import { GameRoundSchema } from '../schemas/game-round.schema';

@Injectable()
export class GameRoundEntityRepository extends BaseEntityRepository<
  GameRoundSchema,
  GameRound
> {
  constructor(
    @InjectModel(GameRoundSchema.name)
    model: Model<GameRoundSchema>,
    schemaFactory: GameRoundSchemaFactory,
  ) {
    super(model, schemaFactory);
  }
}
