import { Model } from 'mongoose';

import { BaseDtoRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { ChallengeLeaderboardSchema } from '../schemas/challenge-leaderboard.schema';

@Injectable()
export class ChallenngeLeaderboardsDtoRepository extends BaseDtoRepository<ChallengeLeaderboardSchema> {
  constructor(
    @InjectModel(ChallengeLeaderboardSchema.name)
    schemaModel: Model<ChallengeLeaderboardSchema>,
  ) {
    super(schemaModel);
  }
}
