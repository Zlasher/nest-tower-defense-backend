import { Model } from 'mongoose';

import { BaseDtoRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { MapMessageSchema } from '../schemas/map-message.schema';

@Injectable()
export class MapMessagesDtoRepository extends BaseDtoRepository<MapMessageSchema> {
  constructor(
    @InjectModel(MapMessageSchema.name)
    schemaModel: Model<MapMessageSchema>,
  ) {
    super(schemaModel);
  }
}
