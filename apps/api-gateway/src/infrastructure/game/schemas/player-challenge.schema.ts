import { IChallenge } from '@app/api/game-api';
import { IdentifiableEntitySchema } from '@app/common/modules';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ versionKey: false, collection: 'challenges' })
export class PlayerChallengeSchema
  extends IdentifiableEntitySchema
  implements IChallenge
{
  @Prop({ required: true })
  playerId: string;

  @Prop({ required: true })
  mapId: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  description: string;

  @Prop({ required: true })
  isPendingCompletion: boolean;

  @Prop({ required: false })
  modifiers: string[];

  @Prop({ required: true })
  createdDate: string;

  @Prop({ required: false })
  excludedTowers?: string[];

  @Prop({ required: false })
  allowedTowers?: string[];
}
