import { IMapMessage } from '@app/api/game-api';
import { IdentifiableEntitySchema } from '@app/common/modules';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ versionKey: false, collection: 'map-messages' })
export class MapMessageSchema
  extends IdentifiableEntitySchema
  implements IMapMessage
{
  @Prop({ required: true })
  mapId: string;

  @Prop({ required: true })
  message: string;

  @Prop({ required: true })
  position: string;

  @Prop({ required: true })
  createdBy: string;

  @Prop({ required: true })
  createdAt: string;
}
