import { IItemCode } from '@app/api/game-api';
import { IdentifiableEntitySchema } from '@app/common/modules';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ versionKey: false, collection: 'itemcodes' })
export class ItemCodeSchema
  extends IdentifiableEntitySchema
  implements IItemCode
{
  @Prop({ required: true })
  code: string;

  @Prop({ required: true })
  itemName: string;

  @Prop({ required: true })
  isUsed: boolean;
}
