import { IChallengeRecord } from '@app/api/game-api/interfaces/IChallengeRecord';
import { IdentifiableEntitySchema } from '@app/common/modules';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ versionKey: false, collection: 'challenge-records' })
export class ChallengeRecordSchema
  extends IdentifiableEntitySchema
  implements IChallengeRecord
{
  @Prop({ required: true })
  playerId: string;

  @Prop({ required: true })
  challengeId: string;

  @Prop({ required: true })
  seconds: number;

  @Prop({ required: true })
  createdDate: string;
}
