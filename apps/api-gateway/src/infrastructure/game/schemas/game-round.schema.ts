import { IGame } from '@app/api/game-api';
import { IdentifiableEntitySchema } from '@app/common/modules';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ versionKey: false, collection: 'game-rounds' })
export class GameRoundSchema extends IdentifiableEntitySchema implements IGame {
  @Prop({ required: true })
  playerId: string;

  @Prop({ required: true })
  mapId: string;

  @Prop({ required: true })
  wavesProgress: string;

  @Prop({ required: true })
  lives: number;

  @Prop({ required: true })
  seconds: number;

  @Prop({ required: true })
  tier: number;

  @Prop({ required: true })
  date: string;

  @Prop({ required: false })
  challengeId?: string;
}
