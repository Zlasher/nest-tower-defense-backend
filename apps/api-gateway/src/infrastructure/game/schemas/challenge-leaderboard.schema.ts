import {
  IChallengeLeaderboard,
  ILeaderboardEntry,
} from '@app/api/game-api/interfaces/IChallengeLeaderboard';
import { IdentifiableEntitySchema } from '@app/common/modules';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ versionKey: false, collection: 'challenge-leaderboards' })
export class ChallengeLeaderboardSchema
  extends IdentifiableEntitySchema
  implements IChallengeLeaderboard
{
  @Prop({ required: true })
  challengeId: string;

  @Prop({ required: true })
  entries: ILeaderboardEntry[];
}
