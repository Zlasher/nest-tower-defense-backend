import { EntitySchemaFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { ChallengeRecord } from '../../../domain/game';
import { ChallengeRecordSchema } from '../schemas/challenge-record.schema';

@Injectable()
export class ChallengeRecordSchemaFactory
  implements EntitySchemaFactory<ChallengeRecordSchema, ChallengeRecord>
{
  create(entity: ChallengeRecord): ChallengeRecordSchema {
    return {
      _id: entity._id,
      playerId: entity.playerId,
      challengeId: entity.challengeId,
      createdDate: entity.createdDate,
      seconds: entity.seconds,
    };
  }

  createFromSchema(schema: ChallengeRecordSchema): ChallengeRecord {
    return new ChallengeRecord({
      _id: schema._id,
      challengeId: schema.challengeId,
      playerId: schema.playerId,
      seconds: schema.seconds,
      createdDate: schema.createdDate,
    });
  }
}
