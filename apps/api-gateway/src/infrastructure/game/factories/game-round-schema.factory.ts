import { EntitySchemaFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { GameRound } from '../../../domain/game';
import { GameRoundSchema } from '../schemas/game-round.schema';

@Injectable()
export class GameRoundSchemaFactory
  implements EntitySchemaFactory<GameRoundSchema, GameRound>
{
  create(entity: GameRound): GameRoundSchema {
    return {
      _id: entity._id,
      playerId: entity.playerId,
      mapId: entity.mapId,
      wavesProgress: entity.wavesProgress,
      lives: entity.lives,
      seconds: entity.seconds,
      tier: entity.tier,
      date: entity.date,
      challengeId: entity.challengeId,
    };
  }

  createFromSchema(schema: GameRoundSchema): GameRound {
    return new GameRound({
      _id: schema._id,
      playerId: schema.playerId,
      mapId: schema.mapId,
      wavesProgress: schema.wavesProgress,
      lives: schema.lives,
      seconds: schema.seconds,
      tier: schema.tier,
      date: schema.date,
      challengeId: schema.challengeId,
    });
  }
}
