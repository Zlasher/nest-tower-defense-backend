import { EntitySchemaFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { MapMessage } from '../../../domain/game';
import { MapMessageSchema } from '../schemas/map-message.schema';

@Injectable()
export class MapMessageSchemaFactory
  implements EntitySchemaFactory<MapMessageSchema, MapMessage>
{
  create(entity: MapMessage): MapMessageSchema {
    return {
      _id: entity._id,
      mapId: entity.mapId,
      message: entity.message,
      position: entity.position,
      createdBy: entity.createdBy,
      createdAt: entity.createdAt,
    };
  }

  createFromSchema(schema: MapMessageSchema): MapMessage {
    return new MapMessage({
      _id: schema._id,
      mapId: schema.mapId,
      message: schema.message,
      position: schema.position,
      createdBy: schema.createdBy,
      createdAt: schema.createdAt,
    });
  }
}
