import { EntitySchemaFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { ChallengeLeaderboard } from '../../../domain/game';
import { ChallengeLeaderboardSchema } from '../schemas/challenge-leaderboard.schema';

@Injectable()
export class ChallengeLeaderboardSchemaFactory
  implements
    EntitySchemaFactory<ChallengeLeaderboardSchema, ChallengeLeaderboard>
{
  create(entity: ChallengeLeaderboard): ChallengeLeaderboardSchema {
    return {
      _id: entity._id,
      challengeId: entity.challengeId,
      entries: entity.entries,
    };
  }

  createFromSchema(schema: ChallengeLeaderboardSchema): ChallengeLeaderboard {
    return new ChallengeLeaderboard({
      _id: schema._id,
      challengeId: schema.challengeId,
      entries: schema.entries,
    });
  }
}
