import { EntitySchemaFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { PlayerChallenge } from '../../../domain/game';
import { PlayerChallengeSchema } from '../schemas/player-challenge.schema';

@Injectable()
export class PlayerChallengeSchemaFactory
  implements EntitySchemaFactory<PlayerChallengeSchema, PlayerChallenge>
{
  create(entity: PlayerChallenge): PlayerChallengeSchema {
    return {
      _id: entity._id,
      playerId: entity.playerId,
      mapId: entity.mapId,
      name: entity.name,
      description: entity.description,
      createdDate: entity.createdDate,
      isPendingCompletion: entity.isPendingCompletion,
      modifiers: entity.modifiers,
      excludedTowers: entity.excludedTowers,
      allowedTowers: entity.allowedTowers,
    };
  }

  createFromSchema(schema: PlayerChallengeSchema): PlayerChallenge {
    return new PlayerChallenge({
      _id: schema._id,
      playerId: schema.playerId,
      mapId: schema.mapId,
      name: schema.name,
      description: schema.description,
      createdDate: schema.createdDate,
      isPendingCompletion: schema.isPendingCompletion,
      modifiers: schema.modifiers,
      excludedTowers: schema.excludedTowers,
      allowedTowers: schema.allowedTowers,
    });
  }
}
