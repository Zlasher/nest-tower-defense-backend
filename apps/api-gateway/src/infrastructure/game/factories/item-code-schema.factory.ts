import { EntitySchemaFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { ItemCode } from '../../../domain/game';
import { ItemCodeSchema } from '../schemas/item-code.schema';

@Injectable()
export class ItemCodeSchemaFactory
  implements EntitySchemaFactory<ItemCodeSchema, ItemCode>
{
  create(entity: ItemCode): ItemCodeSchema {
    return {
      _id: entity._id,
      code: entity.code,
      itemName: entity.itemName,
      isUsed: entity.isUsed,
    };
  }

  createFromSchema(schema: ItemCodeSchema): ItemCode {
    return new ItemCode({
      _id: schema._id,
      code: schema.code,
      itemName: schema.itemName,
      isUsed: schema.isUsed,
    });
  }
}
