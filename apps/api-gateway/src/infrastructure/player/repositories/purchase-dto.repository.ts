import { Model } from 'mongoose';

import { BaseDtoRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { PurchaseSchema } from '../schemas/purchase.schema';

@Injectable()
export class PurchaseDtoRepository extends BaseDtoRepository<PurchaseSchema> {
  constructor(
    @InjectModel(PurchaseSchema.name)
    schemaModel: Model<PurchaseSchema>,
  ) {
    super(schemaModel);
  }
}
