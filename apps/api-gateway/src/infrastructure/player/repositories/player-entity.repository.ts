import { Model } from 'mongoose';

import { BaseEntityRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { Player } from '../../../domain/player';
import { PlayerSchemaFactory } from '../player-schema.factory';
import { PlayerSchema } from '../schemas/player.schema';

@Injectable()
export class PlayerEntityRepository extends BaseEntityRepository<
  PlayerSchema,
  Player
> {
  constructor(
    @InjectModel(PlayerSchema.name)
    playerModel: Model<PlayerSchema>,
    playerSchemaFactory: PlayerSchemaFactory,
  ) {
    super(playerModel, playerSchemaFactory);
  }
}
