import { Model } from 'mongoose';

import { BaseDtoRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { InboxMessageSchema } from '../schemas/inbox-message.schema';

@Injectable()
export class MessageDtoRepository extends BaseDtoRepository<InboxMessageSchema> {
  constructor(
    @InjectModel(InboxMessageSchema.name)
    schemaModel: Model<InboxMessageSchema>,
  ) {
    super(schemaModel);
  }
}
