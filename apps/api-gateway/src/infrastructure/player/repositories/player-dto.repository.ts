import { Model } from 'mongoose';

import { BaseDtoRepository } from '@app/common/modules/database';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { PlayerSchema } from '../schemas/player.schema';

@Injectable()
export class PlayerDtoRepository extends BaseDtoRepository<PlayerSchema> {
  constructor(
    @InjectModel(PlayerSchema.name)
    schemaModel: Model<PlayerSchema>,
  ) {
    super(schemaModel);
  }
}
