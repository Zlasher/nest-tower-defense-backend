import { Model } from 'mongoose';

import { BaseEntityRepository } from '@app/common/modules';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { InboxMessage } from '../../../domain/player';
import { MessageSchemaFactory } from '../message-schema.factory';
import { InboxMessageSchema } from '../schemas/inbox-message.schema';

@Injectable()
export class InboxMessageEntityRepository extends BaseEntityRepository<
  InboxMessageSchema,
  InboxMessage
> {
  constructor(
    @InjectModel(InboxMessageSchema.name)
    model: Model<InboxMessageSchema>,
    schemaFactory: MessageSchemaFactory,
  ) {
    super(model, schemaFactory);
  }
}

export default InboxMessageEntityRepository;
