import { EntitySchemaFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { InboxMessage } from '../../domain/player';
import { InboxMessageSchema } from './schemas/inbox-message.schema';

@Injectable()
export class MessageSchemaFactory
  implements EntitySchemaFactory<InboxMessageSchema, InboxMessage>
{
  create(entity: InboxMessage): InboxMessageSchema {
    return {
      _id: entity._id,
      playerId: entity.playerId,
      message: entity.message,
      date: entity.date,
      isRead: entity.isRead,
    };
  }

  createFromSchema(schema: InboxMessageSchema): InboxMessage {
    return new InboxMessage({
      _id: schema._id,
      playerId: schema.playerId,
      message: schema.message,
      date: schema.date,
      isRead: schema.isRead,
    });
  }
}
