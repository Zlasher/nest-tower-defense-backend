import { Schema, Prop } from '@nestjs/mongoose';
import { IMessageInbox } from '@app/api/player-api';
import { IdentifiableEntitySchema } from '@app/common/modules';

@Schema({ versionKey: false, collection: 'inboxmessages' })
export class InboxMessageSchema
  extends IdentifiableEntitySchema
  implements IMessageInbox
{
  @Prop({ required: true })
  playerId: string;

  @Prop({ required: true })
  message: string;

  @Prop({ default: Date.now().toString() })
  date: string;

  @Prop()
  isRead: boolean;
}
