import { IPurchase } from '@app/api/player-api';
import { IdentifiableEntitySchema } from '@app/common/modules';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ versionKey: false, collection: 'purchases' })
export class PurchaseSchema
  extends IdentifiableEntitySchema
  implements IPurchase
{
  @Prop({ required: true })
  playerId: string;

  @Prop({ required: true })
  itemId: string;

  @Prop({ default: Date.now().toString() })
  date: string;
}
