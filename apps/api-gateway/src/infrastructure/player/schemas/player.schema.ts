import { IPlayer } from '@app/api/player-api';
import { IdentifiableEntitySchema } from '@app/common/modules/database/entity-repository/identifiable-entity.schema';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ versionKey: false, collection: 'players' })
export class PlayerSchema extends IdentifiableEntitySchema implements IPlayer {
  @Prop({ required: true })
  readonly playerId: string;

  @Prop({ required: true })
  readonly name: string;

  @Prop({ required: true })
  readonly device: string;

  @Prop({ default: Date.now().toString() })
  readonly createdDate: string;

  @Prop({ default: Date.now().toString() })
  readonly lastLogin: string;

  @Prop()
  readonly gameVersion: string;

  @Prop()
  readonly isPremium: boolean;

  @Prop()
  readonly discordId: string;
}
