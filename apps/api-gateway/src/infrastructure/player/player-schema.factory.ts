import { Injectable } from '@nestjs/common';

import { EntitySchemaFactory } from '@app/common/modules';
import { PlayerSchema } from '.';
import { Player } from '../../domain/player/entities/Player';

@Injectable()
export class PlayerSchemaFactory
  implements EntitySchemaFactory<PlayerSchema, Player>
{
  create(entity: Player): PlayerSchema {
    return {
      _id: entity._id,
      playerId: entity.playerId,
      name: entity.name,
      device: entity.device,
      createdDate: entity.createdDate,
      lastLogin: entity.lastLogin,
      gameVersion: entity.gameVersion,
      discordId: entity.discordId,
      isPremium: entity.isPremium,
    };
  }

  createFromSchema(schema: PlayerSchema): Player {
    return new Player({
      _id: schema._id,
      playerId: schema.playerId,
      name: schema.name,
      device: schema.device,
      createdDate: schema.createdDate,
      lastLogin: schema.lastLogin,
      gameVersion: schema.gameVersion,
      discordId: schema.discordId,
      isPremium: schema.isPremium,
    });
  }
}
