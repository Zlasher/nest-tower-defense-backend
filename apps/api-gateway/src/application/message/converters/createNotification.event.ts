import {
  ChannelType,
  CreateNotificationDTO,
  CreateNotificationEvent,
} from '@app/api/notification-api';

export const createNotificationEvent = ({
  playerId,
  gameVersion,
  message,
  channelType,
}: CreateNotificationDTO): CreateNotificationEvent => {
  let prefix = `${playerId} - ${gameVersion}`;

  switch (channelType) {
    case ChannelType.Arena:
    case ChannelType.Purchases:
      prefix = '';
  }

  return new CreateNotificationEvent(channelType, `${prefix}\n${message}`);
};
