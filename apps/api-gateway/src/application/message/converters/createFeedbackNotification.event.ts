import {
  ChannelType,
  CreateNotificationDTO,
  CreateNotificationEvent,
} from '@app/api/notification-api';

export const createFeedbackNotificationEvent = ({
  playerId,
  gameVersion,
  message,
}: CreateNotificationDTO): CreateNotificationEvent => {
  return new CreateNotificationEvent(
    ChannelType.Feedback,
    `${playerId} - ${gameVersion}\n\n${message}`,
  );
};
