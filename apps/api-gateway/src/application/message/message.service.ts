import {
  CreateNotificationDTO,
  queues,
  topics,
} from '@app/api/notification-api';
import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

import { createFeedbackNotificationEvent } from './converters/createFeedbackNotification.event';
import { createNotificationEvent } from './converters/createNotification.event';
import { Observable } from 'rxjs';

@Injectable()
export class MessageService {
  constructor(
    @Inject(queues.notifications)
    private readonly notificationClient: ClientProxy,
  ) {}

  createFeedback(payload: CreateNotificationDTO) {
    try {
      this.notificationClient.emit(
        topics.createNotification,
        createFeedbackNotificationEvent(payload),
      );
    } catch (err) {
      throw err;
    }
  }

  createNotification(payload: CreateNotificationDTO): Observable<any> {
    try {
      return this.notificationClient.emit(
        topics.createNotification,
        createNotificationEvent(payload),
      );
    } catch (err) {
      throw err;
    }
  }
}
