import { CreateNotificationDTO } from '@app/api/notification-api';
import { Body, Controller, Post } from '@nestjs/common';

import { MessageService } from './message.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('message')
@ApiTags('messages')
export class MessageController {
  constructor(private readonly messageService: MessageService) {}

  @Post('feedback')
  @ApiResponse({
    status: 200,
    description: 'Created notification successfully',
  })
  createFeedback(@Body() payload: CreateNotificationDTO) {
    return this.messageService.createFeedback(payload);
  }

  @Post('notification')
  @ApiResponse({
    status: 200,
    description: 'Created notification successfully',
  })
  createNotification(@Body() payload: CreateNotificationDTO) {
    return this.messageService.createNotification(payload);
  }
}
