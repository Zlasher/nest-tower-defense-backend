import { CreateChallengeRecordRequest } from '@app/api/game-api';

export class CreateChallengeRecordCommand {
  constructor(public readonly request: CreateChallengeRecordRequest) {}
}
