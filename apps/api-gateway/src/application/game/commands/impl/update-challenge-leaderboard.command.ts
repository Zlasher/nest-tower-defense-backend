import { UpdateChallengeLeaderboardRequest } from '@app/api/game-api';

export class UpdateChallengeLeaderboardCommand {
  constructor(public readonly request: UpdateChallengeLeaderboardRequest) {}
}
