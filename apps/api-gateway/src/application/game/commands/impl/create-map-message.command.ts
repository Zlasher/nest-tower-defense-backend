import { CreateMapMessageRequest } from '@app/api/game-api';

export class CreateMapMessageCommand {
  constructor(public readonly request: CreateMapMessageRequest) {}
}
