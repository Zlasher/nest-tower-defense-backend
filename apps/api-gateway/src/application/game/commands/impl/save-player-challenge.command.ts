import { SaveChallengeRequest } from '@app/api/game-api';

export class SavePlayerChallengeCommand {
  constructor(public readonly request: SaveChallengeRequest) {}
}

export class UpdatePlayerChallengeCommand {
  constructor(public readonly request: SaveChallengeRequest) {}
}
