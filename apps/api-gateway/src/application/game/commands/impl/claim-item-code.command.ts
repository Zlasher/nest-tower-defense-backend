import { ClaimItemCodeRequest } from '@app/api/game-api';

export class ClaimItemCodeCommand {
  constructor(public readonly claimCodeRequest: ClaimItemCodeRequest) {}
}
