import { CreateItemCodesRequest } from '@app/api/game-api';

export class CreateCodesCommand {
  constructor(public readonly createCodesRequest: CreateItemCodesRequest) {}
}
