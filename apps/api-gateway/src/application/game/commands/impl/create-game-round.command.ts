import { CreateGameRoundRequest } from '@app/api/game-api';

export class CreateGameRoundCommand {
  constructor(public readonly createGameRoundRequest: CreateGameRoundRequest) {}
}
