import { ClaimItemCodeHandler } from './handlers/claim-item-code.handler';
import { CreateChallengeRecordHandler } from './handlers/create-challenge-record.handler';
import { CreateGameRoundHandler } from './handlers/create-game-round.handler';
import { CreateCodesHandler } from './handlers/create-item-codes.handler';
import { CreateMapMessageHandler } from './handlers/create-map-message.handler';
import { SavePlayerChallengeHandler } from './handlers/save-player-challenge.handler';
import { UpdateChallengeLeaderboardHandler } from './handlers/update-challenge-leaderboard.handler';
import { UpdatePlayerChallengeHandler } from './handlers/update-player-challenge.handler';

export const GameRoundCommandHandlers = [CreateGameRoundHandler];
export const MapMessageCommandHandlers = [CreateMapMessageHandler];
export const PlayerChallengeCommandHandlers = [
  SavePlayerChallengeHandler,
  UpdatePlayerChallengeHandler,
  CreateChallengeRecordHandler,
  UpdateChallengeLeaderboardHandler,
];
export const ItemCodeCommandHandlers = [
  ClaimItemCodeHandler,
  CreateCodesHandler,
];

export * from './impl/create-game-round.command';
export * from './impl/create-item-codes.command';
export * from './impl/claim-item-code.command';
export * from './impl/save-player-challenge.command';
export * from './impl/create-challenge-record.command';
export * from './impl/update-challenge-leaderboard.command';
export * from './impl/create-map-message.command';
