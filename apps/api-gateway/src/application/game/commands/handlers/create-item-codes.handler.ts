import { ItemCodeFactory } from 'apps/api-gateway/src/domain/game/item-code.factory';

import { CreateItemCodesReponse, ItemCodeDTO } from '@app/api/game-api';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { CreateCodesCommand } from '../impl/create-item-codes.command';

@CommandHandler(CreateCodesCommand)
export class CreateCodesHandler implements ICommandHandler<CreateCodesCommand> {
  constructor(
    private readonly gameRoundFactory: ItemCodeFactory,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({
    createCodesRequest,
  }: CreateCodesCommand): Promise<CreateItemCodesReponse> {
    const codes = await this.gameRoundFactory.create(createCodesRequest);

    for (let i = 0; i < codes.length; i++) {
      this.eventPublisher.mergeObjectContext(codes[i]).commit();
    }

    return {
      ...createCodesRequest,
      codes: codes.map((code) => new ItemCodeDTO(code)),
    };
  }
}
