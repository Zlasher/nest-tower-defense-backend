import { ChallengeLeaderboardFactory } from 'apps/api-gateway/src/domain/game/challenge-leaderboard.factory';
import { ChallengeLeaderboardEntityRepository } from 'apps/api-gateway/src/infrastructure/game/repositories/challenge-leaderboard-entity.repository';

import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { UpdateChallengeLeaderboardCommand } from '../impl/update-challenge-leaderboard.command';

@CommandHandler(UpdateChallengeLeaderboardCommand)
export class UpdateChallengeLeaderboardHandler
  implements ICommandHandler<UpdateChallengeLeaderboardCommand>
{
  constructor(
    private readonly challengeLeaderboardFactory: ChallengeLeaderboardFactory,
    private readonly challengeLeaderboardEntityRepository: ChallengeLeaderboardEntityRepository,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({ request }: UpdateChallengeLeaderboardCommand): Promise<void> {
    const existingDocument =
      await this.challengeLeaderboardEntityRepository.findOneBy({
        challengeId: request.challengeId,
      });

    if (!existingDocument) {
      const newEntity = this.eventPublisher.mergeObjectContext(
        await this.challengeLeaderboardFactory.create(request),
      );

      newEntity.commit();
      return;
    }

    const entity = this.eventPublisher.mergeObjectContext(existingDocument);

    entity.setEntries(request.entries);

    await this.challengeLeaderboardEntityRepository.findOneAndReplaceBy(
      { _id: entity._id },
      entity,
    );

    entity.commit();
  }
}
