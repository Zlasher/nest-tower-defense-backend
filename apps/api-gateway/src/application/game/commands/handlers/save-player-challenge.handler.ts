import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { SavePlayerChallengeCommand } from '../impl/save-player-challenge.command';
import { PlayerChallengeFactory } from 'apps/api-gateway/src/domain/game/player-challenge.factory';
import { PlayerChallengeEntityRepository } from 'apps/api-gateway/src/infrastructure/game';

@CommandHandler(SavePlayerChallengeCommand)
export class SavePlayerChallengeHandler
  implements ICommandHandler<SavePlayerChallengeCommand>
{
  constructor(
    private readonly playerChallengeFactory: PlayerChallengeFactory,
    private readonly playerChallengeEntityRepository: PlayerChallengeEntityRepository,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({ request }: SavePlayerChallengeCommand): Promise<void> {
    const existingDocument =
      await this.playerChallengeEntityRepository.findOneBy({
        playerId: request.playerId,
        mapId: request.mapId,
        isPendingCompletion: true,
      });

    if (!existingDocument) {
      const existingEntity = this.eventPublisher.mergeObjectContext(
        await this.playerChallengeFactory.create(request),
      );

      existingEntity.commit();
      return;
    }

    const entity = this.eventPublisher.mergeObjectContext(existingDocument);

    await this.playerChallengeEntityRepository.findOneAndReplaceBy(
      { _id: entity._id },
      entity,
    );

    entity.commit();
  }
}
