import { ItemCodeEntityRepository } from 'apps/api-gateway/src/infrastructure/game';

import { ClaimItemCodeResponse } from '@app/api/game-api';
import { Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { ClaimItemCodeCommand } from '../impl/claim-item-code.command';

@CommandHandler(ClaimItemCodeCommand)
export class ClaimItemCodeHandler
  implements ICommandHandler<ClaimItemCodeCommand>
{
  protected readonly logger = new Logger(ClaimItemCodeCommand.name);

  constructor(
    private readonly itemCodeEntityRepository: ItemCodeEntityRepository,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({
    claimCodeRequest,
  }: ClaimItemCodeCommand): Promise<ClaimItemCodeResponse> {
    const existingDocument = await this.itemCodeEntityRepository.findOneBy({
      code: claimCodeRequest.code,
      isUsed: false,
    });

    if (!existingDocument) {
      this.logger.warn(
        `Player ${claimCodeRequest.playerId} tried to claim non-existant code ${claimCodeRequest.code}`,
      );
      return {
        ...claimCodeRequest,
        isError: true,
      };
    }

    const entity = this.eventPublisher.mergeObjectContext(existingDocument);

    entity.handleClaim(claimCodeRequest.playerId);

    await this.itemCodeEntityRepository.findOneAndReplaceBy(
      { _id: entity._id },
      entity,
    );

    entity.commit();

    return {
      ...entity,
      isError: false,
    };
  }
}
