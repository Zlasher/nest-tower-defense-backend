import { MapMessageFactory } from 'apps/api-gateway/src/domain/game/map-message.factory';

import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { CreateMapMessageCommand } from '../impl/create-map-message.command';

@CommandHandler(CreateMapMessageCommand)
export class CreateMapMessageHandler
  implements ICommandHandler<CreateMapMessageCommand>
{
  constructor(
    private readonly playerChallengeFactory: MapMessageFactory,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({ request }: CreateMapMessageCommand): Promise<void> {
    const messageEntity = this.eventPublisher.mergeObjectContext(
      await this.playerChallengeFactory.create(request),
    );

    messageEntity.commit();
  }
}
