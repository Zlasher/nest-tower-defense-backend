import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { UpdatePlayerChallengeCommand } from '../impl/save-player-challenge.command';
import { PlayerChallengeFactory } from 'apps/api-gateway/src/domain/game/player-challenge.factory';
import { PlayerChallengeEntityRepository } from 'apps/api-gateway/src/infrastructure/game';

@CommandHandler(UpdatePlayerChallengeCommand)
export class UpdatePlayerChallengeHandler
  implements ICommandHandler<UpdatePlayerChallengeCommand>
{
  constructor(
    private readonly playerChallengeFactory: PlayerChallengeFactory,
    private readonly playerChallengeEntityRepository: PlayerChallengeEntityRepository,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({ request }: UpdatePlayerChallengeCommand): Promise<void> {
    const existingDocument =
      await this.playerChallengeEntityRepository.findOneBy({
        playerId: request.playerId,
      });

    if (!existingDocument) {
      const existingEntity = this.eventPublisher.mergeObjectContext(
        await this.playerChallengeFactory.create(request),
      );

      existingEntity.commit();
      return;
    }

    const entity = this.eventPublisher.mergeObjectContext(existingDocument);

    entity.update(request);

    await this.playerChallengeEntityRepository.findOneAndReplaceBy(
      { _id: entity._id },
      entity,
    );

    entity.commit();
  }
}
