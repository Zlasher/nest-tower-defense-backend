import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { CreateChallengeRecordCommand } from '../impl/create-challenge-record.command';
import { ChallengeRecordFactory } from 'apps/api-gateway/src/domain/game/challenge-record.factory';
import { PlayerChallengeEntityRepository } from 'apps/api-gateway/src/infrastructure/game';
import { Logger } from '@nestjs/common';

@CommandHandler(CreateChallengeRecordCommand)
export class CreateChallengeRecordHandler
  implements ICommandHandler<CreateChallengeRecordCommand>
{
  protected readonly logger = new Logger(CreateChallengeRecordHandler.name);

  constructor(
    private readonly challengeRecordFactory: ChallengeRecordFactory,
    private readonly playerChallengeEntityRepository: PlayerChallengeEntityRepository,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({ request }: CreateChallengeRecordCommand): Promise<void> {
    const messageEntity = this.eventPublisher.mergeObjectContext(
      await this.challengeRecordFactory.create(request),
    );

    messageEntity.commit();

    const existingDocument =
      await this.playerChallengeEntityRepository.findOneBy({
        playerId: request.playerId,
        _id: request.challengeId,
        isPendingCompletion: true,
      });

    if (!existingDocument) {
      return;
    }

    const entity = this.eventPublisher.mergeObjectContext(existingDocument);
    entity.setActive();

    await this.playerChallengeEntityRepository.findOneAndReplaceBy(
      { _id: entity._id },
      entity,
    );

    entity.commit();
  }
}
