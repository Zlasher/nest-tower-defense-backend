import { GameRoundFactory } from 'apps/api-gateway/src/domain/game/game-round.factory';

import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { CreateGameRoundCommand } from '../impl/create-game-round.command';

@CommandHandler(CreateGameRoundCommand)
export class CreateGameRoundHandler
  implements ICommandHandler<CreateGameRoundCommand>
{
  constructor(
    private readonly gameRoundFactory: GameRoundFactory,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({
    createGameRoundRequest,
  }: CreateGameRoundCommand): Promise<void> {
    const messageEntity = this.eventPublisher.mergeObjectContext(
      await this.gameRoundFactory.create(createGameRoundRequest),
    );

    messageEntity.commit();
  }
}
