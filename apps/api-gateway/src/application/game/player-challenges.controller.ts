import {
  ChallengeLeaderboardsQuery,
  ChallengeLeaderboardsResponse,
  ChallengesQuery,
  ChallengesResponse,
  PlayerChallengeDto,
  SaveChallengeRequest,
  UpdateChallengeLeaderboardRequest,
} from '@app/api/game-api';
import { Body, Controller, Post } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { ChallengesService } from './services';
import { DailyChallengeCacheService } from './services/daily-challenges/challenge.cache.service';
import { DailyChallengeService } from './services/daily-challenges/daily-challenge.service';
import { LeaderboardAggregationService } from './services/leaderboard-aggregation.service';

@Controller('challenges')
@ApiTags('challenges')
export class PlayerChallengesController {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly aggregationService: LeaderboardAggregationService,
    private readonly challengesService: ChallengesService,
    private readonly dailyChallengeService: DailyChallengeService,
    private readonly dailyChallengeCacheService: DailyChallengeCacheService,
  ) {}

  @Post()
  @ApiResponse({
    status: 200,
    description: 'Fetched challenges successfully',
    type: ChallengesResponse,
  })
  async getChallenges(
    @Body() query: ChallengesQuery,
  ): Promise<ChallengesResponse> {
    return this.dailyChallengeCacheService
      .queryWithCache(new ChallengesQuery().copy(query))
      .then((challenges) => ({ challenges }));
  }

  @Post('save')
  @ApiResponse({
    status: 203,
    description: 'Created player-challenge entry successfully',
  })
  async saveChallenge(@Body() payload: SaveChallengeRequest): Promise<void> {
    return this.challengesService.createChallenge(payload);
  }

  @Post('regenerate-daily-challenges')
  @ApiResponse({
    isArray: true,
    status: 203,
    description: 'Created daily challenge entry successfully',
    type: PlayerChallengeDto,
  })
  @Cron(CronExpression.EVERY_DAY_AT_2AM)
  async regenerateDailyChallenges(): Promise<PlayerChallengeDto[]> {
    const query = new ChallengesQuery({ name: 'DAILY_CHALLENGE' });

    return this.dailyChallengeCacheService.thenCache(
      query.name,
      this.dailyChallengeService.regenerateDailyChallenges(query),
    );
  }

  @Post('/leaderboards/aggregate')
  @ApiResponse({
    isArray: true,
    status: 203,
    description: 'Leaderboards aggregated successfully',
    type: UpdateChallengeLeaderboardRequest,
  })
  async aggregateLeaderboards(): Promise<UpdateChallengeLeaderboardRequest[]> {
    return this.aggregationService.aggregateAllChallenges();
  }

  @Post('/leaderboards')
  @ApiResponse({
    status: 200,
    description: 'Fetched leaderboards successfully',
    type: ChallengeLeaderboardsResponse,
  })
  async getLeaderboards(
    @Body() payload: ChallengeLeaderboardsQuery,
  ): Promise<ChallengeLeaderboardsResponse> {
    const response = await this.queryBus.execute<
      ChallengeLeaderboardsQuery,
      ChallengeLeaderboardsResponse
    >(new ChallengeLeaderboardsQuery(payload));

    const playerNameRecord: Record<string, string> =
      await this.challengesService.getPlayersInLeaderboards(
        response.leaderboards,
      );

    response.leaderboards.forEach((leaderboard) =>
      leaderboard.entries.forEach((entry) => {
        entry.playerId = playerNameRecord[entry.playerId] || 'Unknown';
      }),
    );

    return response;
  }
}
