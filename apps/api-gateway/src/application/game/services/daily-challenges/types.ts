export interface IDailyChallengeConfig {
  id: string;
  numberOfModifers: number;
  numberOfExcludedTowers: number;
}
