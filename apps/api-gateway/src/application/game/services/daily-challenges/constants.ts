import { IDailyChallengeConfig } from './types';

export const DailyChallengeConfigs: IDailyChallengeConfig[] = [
  {
    id: 'LEVEL_FORGOTTEN_DUNGEONS_DRAGON_CHALLENGE',
    numberOfModifers: 11,
    numberOfExcludedTowers: 4,
  },
  {
    id: 'BEGINNER_CHALLENGE',
    numberOfModifers: 5,
    numberOfExcludedTowers: 2,
  },
];
