import { MODIFIERS, TOWERS, generateLevels } from '@app/api/game-api';

export default class RandomGeneratorHandler {
  private readonly levelPool: string[];

  constructor() {
    this.levelPool = generateLevels();
  }

  public getRandomModifiers(takeCount: number): string[] {
    return this.getRandomsFrom(takeCount, MODIFIERS);
  }

  public getRandomTowers(takeCount: number): string[] {
    return this.getRandomsFrom(takeCount, TOWERS);
  }

  public getRandomLevel(): string {
    return this.getRandomFrom(this.levelPool);
  }

  private getRandomsFrom(takeCount: number, array: string[]): string[] {
    const picked: string[] = [];

    let tries = 0;
    while (picked.length < takeCount && tries < array.length) {
      const item = this.getRandomFrom(array);

      if (picked.includes(item)) {
        tries++;
        continue;
      }

      picked.push(item);
    }

    return picked;
  }

  private getRandomFrom(array: string[]): string {
    const pickedIndex = Math.random() * (array.length - 1);

    return array[Math.floor(pickedIndex)];
  }
}
