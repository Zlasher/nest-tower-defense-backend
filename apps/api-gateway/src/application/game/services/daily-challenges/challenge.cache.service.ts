import { Cache, CachingConfig } from 'cache-manager';

import { ChallengesQuery, PlayerChallengeDto } from '@app/api/game-api';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Inject, Logger } from '@nestjs/common';

import { ChallengesService } from '../challenges.service';

const cacheConfig: CachingConfig = {
  ttl: 60 * 60 * 24, // 24 Hours Cache.
};

export class DailyChallengeCacheService {
  protected readonly logger = new Logger(DailyChallengeCacheService.name);

  constructor(
    @Inject(CACHE_MANAGER)
    private readonly cacheManager: Cache,
    private readonly challengesService: ChallengesService,
  ) {}

  public async queryWithCache(
    query: ChallengesQuery,
  ): Promise<PlayerChallengeDto[]> {
    try {
      const challenges = await this.getCached(query.name);

      if (challenges?.length) {
        return challenges;
      }
    } catch (error) {
      this.logger.error(`Error fetching cache for ${query.name}`, error);
    }

    return this.thenCache(
      query.name,
      this.challengesService
        .find(query)
        .then((response) => response.challenges),
    );
  }

  public getCached(key: string): Promise<PlayerChallengeDto[]> {
    return this.cacheManager.get<PlayerChallengeDto[]>(key).then((c) => c);
  }

  public thenCache(
    key: string,
    promise: Promise<PlayerChallengeDto[]>,
  ): Promise<PlayerChallengeDto[]> {
    return promise.then((challenges) =>
      this.cacheManager
        .set<PlayerChallengeDto[]>(key, challenges, cacheConfig)
        .then((c) => c),
    );
  }
}
