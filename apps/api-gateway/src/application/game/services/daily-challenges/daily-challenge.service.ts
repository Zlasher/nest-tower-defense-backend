import {
  ChallengesQuery,
  PlayerChallengeDto,
  SaveChallengeRequest,
} from '@app/api/game-api';
import {
  CreateNotificationDTO,
  CreateNotificationEvent,
  INotificationEvent,
} from '@app/api/notification-api';
import { Injectable, Logger } from '@nestjs/common';

import { ChallengesService } from '../challenges.service';
import { DailyChallengeConfigs } from './constants';
import RandomGeneratorHandler from './random-generator.handler';

@Injectable()
export class DailyChallengeService
  implements INotificationEvent<CreateNotificationEvent>
{
  private readonly randomGeneratorHandler: RandomGeneratorHandler;

  protected readonly logger = new Logger(DailyChallengeService.name);

  constructor(private readonly challengesService: ChallengesService) {
    this.randomGeneratorHandler = new RandomGeneratorHandler();
  }

  private createUpdatePayloadsForChallenges(): SaveChallengeRequest[] {
    return DailyChallengeConfigs.map((config) => {
      return {
        playerId: config.id,
        mapId: this.randomGeneratorHandler.getRandomLevel(),
        name: 'DAILY_CHALLENGE',
        description: 'Daily Challenge',
        modifiers: this.randomGeneratorHandler.getRandomModifiers(
          config.numberOfModifers,
        ),
        excludedTowers: this.randomGeneratorHandler.getRandomTowers(
          config.numberOfExcludedTowers,
        ),
        allowedTowers: [],
      };
    });
  }

  public async regenerateDailyChallenges(
    query: ChallengesQuery,
  ): Promise<PlayerChallengeDto[]> {
    this.logger.log('Regenerating daily challenges...');

    try {
      for (const payload of this.createUpdatePayloadsForChallenges()) {
        await this.challengesService.createOrUpdateChallenge(payload);
      }

      return this.challengesService
        .find(new ChallengesQuery(query))
        .then((response) => response.challenges);
    } catch (error) {
      this.logger.error(error);
    }

    return [];
  }

  public createNotification(
    event: CreateNotificationEvent,
  ): CreateNotificationDTO {
    return new CreateNotificationDTO(
      'SCHEDULE_SERVICE',
      event.message,
      '',
      event.channelType,
    );
  }
}
