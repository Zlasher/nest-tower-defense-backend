export interface RoundFailedMap {
  mapId: string;
  failedCount: number;
}

export interface RoundsReport {
  report: string;
  totalPlayed: number;
}
