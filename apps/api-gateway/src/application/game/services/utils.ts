import { GameRoundDTO, IGame } from '@app/api/game-api';

import { RoundFailedMap, RoundsReport } from './types';

export const expensiveCalculationMostFailed = (
  rounds: IGame[],
): RoundFailedMap[] => {
  const data: Record<string, number> = rounds.reduce((acc, round) => {
    if (round.lives > 0) {
      return acc;
    }

    if (!acc[round.mapId]) {
      acc[round.mapId] = 0;
    }

    acc[round.mapId] += 1;
    return acc;
  }, {});

  return Object.keys(data)
    .map((key) => ({
      mapId: key,
      failedCount: data[key],
    }))
    .sort((a, b) => b.failedCount - a.failedCount);
};

export const createRoundsReport = (
  gameRounds: GameRoundDTO[],
): RoundsReport => {
  try {
    const report = expensiveCalculationMostFailed(gameRounds)
      .map(
        ({ mapId, failedCount }) =>
          `${mapId.split('LEVEL_')[1]} ---> ${failedCount}\n`,
      )
      .slice(0, 15)
      .join('');

    return {
      report,
      totalPlayed: gameRounds.length,
    };
  } catch (error) {
    return {
      report: error?.message || error || 'Failed to generate report',
      totalPlayed: 0,
    };
  }
};
