import { GameRoundsQuery, GameRoundsResponse } from '@app/api/game-api';
import {
  ChannelType,
  CreateNotificationDTO,
  CreateNotificationEvent,
  INotificationEvent,
} from '@app/api/notification-api';
import { Injectable, Logger } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { Cron, CronExpression } from '@nestjs/schedule';

import { MessageService } from '../../message';
import { RoundsReport } from './types';
import { createRoundsReport } from './utils';

export const SECONDS_IN_1_DAY = 86400;

@Injectable()
export class RoundsReportService
  implements INotificationEvent<CreateNotificationEvent>
{
  protected readonly logger = new Logger(RoundsReportService.name);

  constructor(
    private readonly messageService: MessageService,
    private readonly queryBus: QueryBus,
  ) {}

  private fetchRounds(query: GameRoundsQuery): Promise<GameRoundsResponse> {
    return this.queryBus.execute<GameRoundsQuery, GameRoundsResponse>(
      new GameRoundsQuery().copy(query),
    );
  }

  @Cron(CronExpression.EVERY_DAY_AT_8PM)
  async handleDailyCron(): Promise<RoundsReport> {
    this.logger.log('Running Daily cron...');

    const secondsBackInTime = Date.now() - SECONDS_IN_1_DAY * 7 * 1000;

    return this.generateReportFor(
      new GameRoundsQuery().copy({
        date: secondsBackInTime.toString(),
        state: 'lose',
        tier: 2,
      }),
    );
  }

  public async generateReportFor(
    query: GameRoundsQuery,
  ): Promise<RoundsReport> {
    return this.fetchRounds(query).then((response) => {
      const report = createRoundsReport(response.rounds);
      this.publishMessage(report, 'Total rounds: ');

      return report;
    });
  }

  private publishMessage(
    { totalPlayed, report }: RoundsReport,
    message: string,
  ) {
    try {
      this.messageService.createNotification(
        this.createNotification(
          new CreateNotificationEvent(
            ChannelType.Notification,
            `\n${message} ${totalPlayed}\n${report}`,
          ),
        ),
      );
    } catch (error) {
      this.messageService.createNotification(
        this.createNotification(
          new CreateNotificationEvent(
            ChannelType.Notification,
            `Cron Failed: ${error || 'Something went wrong'}`,
          ),
        ),
      );
    }
  }

  public createNotification(
    event: CreateNotificationEvent,
  ): CreateNotificationDTO {
    return new CreateNotificationDTO(
      'SCHEDULE_SERVICE',
      event.message,
      '',
      event.channelType,
    );
  }
}
