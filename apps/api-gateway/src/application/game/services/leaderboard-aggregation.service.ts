import {
  ChallengesQuery,
  UpdateChallengeLeaderboardRequest,
} from '@app/api/game-api';
import { ChallengeRecordDto } from '@app/api/game-api/dto/challenge-record.dto';
import { ChallengeRecordsQuery } from '@app/api/game-api/dto/queries/challenge-records.query';
import {
  CreateNotificationDTO,
  CreateNotificationEvent,
  INotificationEvent,
} from '@app/api/notification-api';
import { Injectable, Logger } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { Cron, CronExpression } from '@nestjs/schedule';

import { UpdateChallengeLeaderboardCommand } from '../commands';
import { ChallengesService } from './challenges.service';
import { ILeaderboardEntry } from '@app/api/game-api/interfaces/IChallengeLeaderboard';

@Injectable()
export class LeaderboardAggregationService
  implements INotificationEvent<CreateNotificationEvent>
{
  protected readonly logger = new Logger(LeaderboardAggregationService.name);

  constructor(
    private readonly challengesService: ChallengesService,
    private readonly commandBus: CommandBus,
  ) {}

  @Cron(CronExpression.EVERY_HOUR)
  public async aggregateAllChallenges(): Promise<
    UpdateChallengeLeaderboardRequest[]
  > {
    this.logger.log('Aggregating Leaderboards');

    const { challenges } = await this.challengesService.find(
      new ChallengesQuery(),
    );

    const sentRequests: UpdateChallengeLeaderboardRequest[] = [];
    const promises: Promise<void>[] = [];

    for (const { challengeId, playerId } of challenges) {
      const recordResponse = await this.challengesService.findRecords(
        new ChallengeRecordsQuery({ challengeId }),
      );

      const entries: ILeaderboardEntry[] = this.getBestRecordPerPlayer(
        recordResponse.records,
      )
        .sort((a, b) => a.seconds - b.seconds)
        .map(({ playerId, seconds }) => ({ playerId, seconds }));

      const updateRequest: UpdateChallengeLeaderboardRequest = {
        challengeId,
        entries,
      };

      promises.push(
        this.commandBus.execute<UpdateChallengeLeaderboardCommand, void>(
          new UpdateChallengeLeaderboardCommand(updateRequest),
        ),
      );

      sentRequests.push(updateRequest);

      this.logger.log(
        `Challenge ${playerId} (${challengeId}) had ${entries.length} entries.`,
      );
    }

    return Promise.all(promises).then(() => sentRequests);
  }

  private getBestRecordPerPlayer(
    records: ChallengeRecordDto[],
  ): ChallengeRecordDto[] {
    const bestRecords = records.reduce((acc, record) => {
      const key = record.playerId;

      if (!acc[key]) {
        acc[key] = {
          seconds: Number.POSITIVE_INFINITY,
        };
      }

      if (record.seconds < acc[key].seconds) {
        acc[key] = record;
      }

      return acc;
    }, {});

    return Object.values(bestRecords);
  }

  public createNotification(
    event: CreateNotificationEvent,
  ): CreateNotificationDTO {
    return new CreateNotificationDTO(
      'SCHEDULE_SERVICE',
      event.message,
      '',
      event.channelType,
    );
  }
}
