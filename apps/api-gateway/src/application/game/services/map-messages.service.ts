import { CreateMapMessageRequest, MapMessageDto } from '@app/api/game-api';
import {
  GetMessageByMapQuery,
  GetMessageByMapResponse,
} from '@app/api/game-api/dto/queries/map-messages-by-map';
import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';

import { CreateMapMessageCommand } from '../commands';

@Injectable()
export class MapMessagesService {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
  ) {}

  public async getMessagesForMap(
    query: GetMessageByMapQuery,
  ): Promise<GetMessageByMapResponse> {
    const messages = await this.queryBus.execute<
      GetMessageByMapQuery,
      MapMessageDto[]
    >(query);

    if (!messages.length) {
      return {
        messageDto: null,
      };
    }

    const randomIndex = Math.random() * (messages.length - 1);

    return {
      messageDto: messages[randomIndex] || null,
    };
  }

  public createMessageForMap(payload: CreateMapMessageRequest) {
    return this.commandBus.execute<CreateMapMessageCommand, void>(
      new CreateMapMessageCommand(payload),
    );
  }
}
