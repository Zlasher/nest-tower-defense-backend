import {
  ChallengeLeaderboardDto,
  ChallengesQuery,
  ChallengesResponse,
  CreateChallengeRecordRequest,
  SaveChallengeRequest,
} from '@app/api/game-api';
import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { GameRound } from 'apps/api-gateway/src/domain/game';
import {
  CreateChallengeRecordCommand,
  SavePlayerChallengeCommand,
  UpdatePlayerChallengeCommand,
} from '../commands';
import {
  ChallengeRecordsQuery,
  ChallengeRecordsResponse,
} from '@app/api/game-api/dto/queries/challenge-records.query';
import { PlayerService } from '../../player';

@Injectable()
export class ChallengesService {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
    private readonly playerService: PlayerService,
  ) {}

  public find(query: ChallengesQuery): Promise<ChallengesResponse> {
    return this.queryBus.execute<ChallengesQuery, ChallengesResponse>(query);
  }

  public createChallenge(payload: SaveChallengeRequest): Promise<void> {
    return this.commandBus.execute<SavePlayerChallengeCommand, void>(
      new SavePlayerChallengeCommand(payload),
    );
  }

  public createOrUpdateChallenge(payload: SaveChallengeRequest): Promise<void> {
    return this.commandBus.execute<UpdatePlayerChallengeCommand, void>(
      new UpdatePlayerChallengeCommand(payload),
    );
  }

  public findRecords(
    query: ChallengeRecordsQuery,
  ): Promise<ChallengeRecordsResponse> {
    return this.queryBus.execute<
      ChallengeRecordsQuery,
      ChallengeRecordsResponse
    >(new ChallengeRecordsQuery(query));
  }

  public async createRecord({
    challengeId,
    playerId,
    seconds,
  }: GameRound): Promise<void> {
    const createRequest = new CreateChallengeRecordRequest(
      challengeId,
      playerId,
      seconds,
    );

    this.commandBus
      .execute<CreateChallengeRecordCommand, void>(
        new CreateChallengeRecordCommand(createRequest),
      )
      .catch(console.error);
  }

  public async getPlayersInLeaderboards(
    leaderboards: ChallengeLeaderboardDto[],
  ): Promise<Record<string, string>> {
    const playerPromises: Promise<void>[] = [];
    const playerNameRecord: Record<string, string> = {};

    leaderboards.forEach((leaderboard) =>
      leaderboard.entries.forEach((entry) => {
        const playerPromise = this.playerService
          .getPlayerById({ playerId: entry.playerId })
          .then((player) => {
            if (player?.playerId) {
              playerNameRecord[player.playerId] = player.name || 'Unknown';
            }
          });

        playerPromises.push(playerPromise);
      }),
    );

    return Promise.all(playerPromises).then(() => playerNameRecord);
  }
}
