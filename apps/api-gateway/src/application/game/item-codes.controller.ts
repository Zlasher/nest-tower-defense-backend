import {
  ClaimItemCodeRequest,
  ClaimItemCodeResponse,
  CreateItemCodesReponse,
  CreateItemCodesRequest,
  ItemCodeDTO,
} from '@app/api/game-api';
import { Body, Controller, Post } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { ClaimItemCodeCommand, CreateCodesCommand } from './commands';
import { ItemCodesQuery } from './queries';

@Controller('item-codes')
@ApiTags('game')
export class ItemCodesController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post()
  @ApiResponse({
    status: 200,
    description: 'Item-codes fetched successfully',
    isArray: true,
    type: ItemCodeDTO,
  })
  getAllCodes(): Promise<ItemCodeDTO[]> {
    return this.queryBus.execute<ItemCodesQuery, ItemCodeDTO[]>(
      new ItemCodesQuery(),
    );
  }

  @Post('claim')
  @ApiResponse({
    status: 200,
    description: 'Item-code claimed successfully',
    type: ClaimItemCodeResponse,
  })
  claimCode(
    @Body() payload: ClaimItemCodeRequest,
  ): Promise<ClaimItemCodeResponse> {
    return this.commandBus.execute<ClaimItemCodeCommand, ClaimItemCodeResponse>(
      new ClaimItemCodeCommand(payload),
    );
  }

  @Post('add')
  @ApiResponse({
    status: 200,
    description: 'Item-codes created successfully',
    type: CreateItemCodesReponse,
  })
  addCodes(
    @Body() payload: CreateItemCodesRequest,
  ): Promise<CreateItemCodesReponse> {
    return this.commandBus.execute<CreateCodesCommand, CreateItemCodesReponse>(
      new CreateCodesCommand(payload),
    );
  }
}
