import { FilterQuery } from 'mongoose';

import {
  ChallengeLeaderboardDto,
  ChallengeLeaderboardsQuery,
  ChallengeLeaderboardsResponse,
} from '@app/api/game-api';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ChallengeLeaderboard } from 'apps/api-gateway/src/domain/game';
import { ChallenngeLeaderboardsDtoRepository } from 'apps/api-gateway/src/infrastructure/game/repositories/challenge-leaderboard-dto.repository';
import { IChallengeLeaderboard } from '@app/api/game-api/interfaces/IChallengeLeaderboard';

@QueryHandler(ChallengeLeaderboardsQuery)
export class ChallengeLeaderboardHandler
  implements IQueryHandler<ChallengeLeaderboardsQuery>
{
  constructor(
    private readonly dtoRepository: ChallenngeLeaderboardsDtoRepository,
  ) {}

  async execute(
    query: ChallengeLeaderboardsQuery,
  ): Promise<ChallengeLeaderboardsResponse> {
    const filterQuery: FilterQuery<ChallengeLeaderboard> = query.challengeId
      ? {
          challengeId: query.challengeId,
        }
      : {};

    return this.dtoRepository
      .find(filterQuery)
      .then((leaderboards: IChallengeLeaderboard[]) => ({
        leaderboards: (leaderboards || []).map(
          (leaderboard) => new ChallengeLeaderboardDto(leaderboard),
        ),
      }));
  }
}
