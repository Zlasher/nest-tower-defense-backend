import { ChallengeRecord } from 'apps/api-gateway/src/domain/game';
import { ChallenngeRecordsDtoRepository } from 'apps/api-gateway/src/infrastructure/game';
import { FilterQuery } from 'mongoose';

import { ChallengeRecordDto } from '@app/api/game-api/dto/challenge-record.dto';
import {
  ChallengeRecordsQuery,
  ChallengeRecordsResponse,
} from '@app/api/game-api/dto/queries/challenge-records.query';
import { IChallengeRecord } from '@app/api/game-api/interfaces/IChallengeRecord';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';

@QueryHandler(ChallengeRecordsQuery)
export class ChallengeRecordsHandler
  implements IQueryHandler<ChallengeRecordsQuery>
{
  constructor(private readonly dtoRepository: ChallenngeRecordsDtoRepository) {}

  async execute(
    query: ChallengeRecordsQuery,
  ): Promise<ChallengeRecordsResponse> {
    const filterQuery: FilterQuery<ChallengeRecord> = {
      challengeId: query.challengeId,
    };

    return this.dtoRepository
      .find(filterQuery)
      .then((records: IChallengeRecord[]) => ({
        records: (records || []).map(
          (record) => new ChallengeRecordDto(record),
        ),
      }));
  }
}
