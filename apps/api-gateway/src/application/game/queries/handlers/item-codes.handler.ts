import { ItemCodesDtoRepository } from 'apps/api-gateway/src/infrastructure/game';

import { IItemCode, ItemCodeDTO } from '@app/api/game-api';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';

import { ItemCodesQuery } from '../impl/item-codes.query';

@QueryHandler(ItemCodesQuery)
export class ItemCodesQueryHandler implements IQueryHandler<ItemCodesQuery> {
  constructor(
    private readonly itemCodesDtoRepository: ItemCodesDtoRepository,
  ) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async execute(_query: ItemCodesQuery): Promise<ItemCodeDTO[]> {
    return this.itemCodesDtoRepository
      .find({ isUsed: false })
      .then((codes) =>
        codes.map((code: IItemCode) => new ItemCodeDTO(code)).reverse(),
      );
  }
}
