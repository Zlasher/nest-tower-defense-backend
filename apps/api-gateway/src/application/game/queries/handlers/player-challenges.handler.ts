import {
  PlayerChallengeSchema,
  PlayerChallengesDtoRepository,
} from 'apps/api-gateway/src/infrastructure/game';
import { FilterQuery } from 'mongoose';

import {
  ChallengesQuery,
  ChallengesResponse,
  IChallenge,
  PlayerChallengeDto,
} from '@app/api/game-api';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';

@QueryHandler(ChallengesQuery)
export class PlayerChallengesHandler implements IQueryHandler<ChallengesQuery> {
  constructor(private readonly dtoRepository: PlayerChallengesDtoRepository) {}

  async execute(query: ChallengesQuery): Promise<ChallengesResponse> {
    const filterQuery: FilterQuery<PlayerChallengeSchema> = {};

    if (query.playerId) {
      filterQuery.playerId = query.playerId;
    }

    if (query.name) {
      filterQuery.name = query.name;
    }

    if (query.mapId) {
      filterQuery.mapId = query.mapId;
    }

    return this.dtoRepository
      .find(filterQuery)
      .then((challenges: IChallenge[]) => ({
        challenges: challenges.map(
          (challenge) => new PlayerChallengeDto(challenge),
        ),
      }));
  }
}
