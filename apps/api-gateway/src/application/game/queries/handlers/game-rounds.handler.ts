import {
  GameRoundSchema,
  GameRoundsDtoRepository,
} from 'apps/api-gateway/src/infrastructure/game';

import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import {
  GameRoundDTO,
  GameRoundsQuery,
  GameRoundsResponse,
  IGame,
} from '@app/api/game-api';
import { FilterQuery } from 'mongoose';

@QueryHandler(GameRoundsQuery)
export class GameRoundsHandler implements IQueryHandler<GameRoundsQuery> {
  constructor(
    private readonly gameRoundsDtoRepository: GameRoundsDtoRepository,
  ) {}

  async execute(query: GameRoundsQuery): Promise<GameRoundsResponse> {
    const filterQuery: FilterQuery<GameRoundSchema> = {
      date: query.date ? { $gte: query.date } : { $gte: '0' },
    };

    if (query.state) {
      filterQuery.lives = query.state === 'win' ? { $gte: 1 } : { $lte: 0 };
    }

    if (query.tier) {
      filterQuery.tier = query.tier;
    }

    return this.gameRoundsDtoRepository
      .find(filterQuery)
      .then((rounds: IGame[]) => ({
        ...query,
        count: rounds.length,
        rounds: rounds.map((round) => new GameRoundDTO(round)),
      }));
  }
}
