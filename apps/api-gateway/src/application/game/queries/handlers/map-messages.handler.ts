import { MapMessagesDtoRepository } from 'apps/api-gateway/src/infrastructure/game';

import { IMapMessage, MapMessageDto } from '@app/api/game-api';
import { GetMessageByMapQuery } from '@app/api/game-api/dto/queries/map-messages-by-map';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';

@QueryHandler(GetMessageByMapQuery)
export class MapMessagesHandler implements IQueryHandler<GetMessageByMapQuery> {
  constructor(private readonly respository: MapMessagesDtoRepository) {}

  async execute({ mapId }: GetMessageByMapQuery): Promise<MapMessageDto[]> {
    return this.respository
      .find({ mapId })
      .then((messages) =>
        (messages || []).map((code: IMapMessage) => new MapMessageDto(code)),
      )
      .catch(() => []);
  }
}
