import { GameRoundsDtoRepository } from 'apps/api-gateway/src/infrastructure/game';

import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GameRoundsByPlayerAndMapQuery } from '../impl/game-rounds.query';
import {
  GameRoundDTO,
  GameRoundsByPlayerAndMapResponse,
  IGame,
} from '@app/api/game-api';

@QueryHandler(GameRoundsByPlayerAndMapQuery)
export class GameRoundsByPlayerAndMapHandler
  implements IQueryHandler<GameRoundsByPlayerAndMapQuery>
{
  constructor(
    private readonly gameRoundsDtoRepository: GameRoundsDtoRepository,
  ) {}

  async execute(
    query: GameRoundsByPlayerAndMapQuery,
  ): Promise<GameRoundsByPlayerAndMapResponse> {
    return this.gameRoundsDtoRepository.find(query).then((rounds: IGame[]) => ({
      ...query,
      count: rounds.length,
      rounds: rounds.map((round) => new GameRoundDTO(round)),
    }));
  }
}
