import { GameRoundsByPlayerAndMapHandler } from './handlers/player-game-rounds.handler';
import { ItemCodesQueryHandler } from './handlers/item-codes.handler';
import { PlayerChallengesHandler } from './handlers/player-challenges.handler';
import { GameRoundsHandler } from './handlers/game-rounds.handler';
import { ChallengeLeaderboardHandler } from './handlers/challenge-leaderboards.handler';
import { ChallengeRecordsHandler } from './handlers/challenge-records.handler';
import { MapMessagesHandler } from './handlers/map-messages.handler';

export const GameRoundsQueryHandlers = [
  GameRoundsByPlayerAndMapHandler,
  GameRoundsHandler,
];

export const MapMessagesQueryHandlers = [MapMessagesHandler];

export const PlayerChallengeQueryHandlers = [
  PlayerChallengesHandler,
  ChallengeLeaderboardHandler,
  ChallengeRecordsHandler,
];

export const ItemCodesQueryHandlers = [ItemCodesQueryHandler];

export * from './impl/game-rounds.query';
export * from './impl/item-codes.query';
