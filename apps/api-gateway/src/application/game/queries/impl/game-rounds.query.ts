import { IsNotEmpty, IsString } from 'class-validator';

import { PlayerAndMapQuery } from '@app/api/game-api';

export class GameRoundsByPlayerAndMapQuery {
  @IsString()
  @IsNotEmpty()
  public readonly playerId: string;

  @IsString()
  @IsNotEmpty()
  public readonly mapId: string;

  constructor(query: PlayerAndMapQuery) {
    this.playerId = query.playerId;
    this.mapId = query.mapId;
  }
}
