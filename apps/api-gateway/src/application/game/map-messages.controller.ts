import { CreateMapMessageRequest } from '@app/api/game-api';
import {
  GetMessageByMapQuery,
  GetMessageByMapResponse,
} from '@app/api/game-api/dto/queries/map-messages-by-map';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { MapMessagesService } from './services/map-messages.service';

@Controller('map-messages')
@ApiTags('map-messages')
export class MapMessagesController {
  constructor(private readonly mapMessagesService: MapMessagesService) {}

  @Post()
  @ApiResponse({
    status: 200,
    description: 'Map message fetched successfully',
    type: GetMessageByMapResponse,
  })
  getMessageForMap(
    @Body() { mapId }: GetMessageByMapQuery,
  ): Promise<GetMessageByMapResponse> {
    return this.mapMessagesService.getMessagesForMap(
      new GetMessageByMapQuery(mapId),
    );
  }

  @Post('create')
  @ApiResponse({
    status: 203,
    description: 'Created map message successfully',
  })
  createMessageForMap(@Body() payload: CreateMapMessageRequest) {
    return this.mapMessagesService.createMessageForMap(payload);
  }
}
