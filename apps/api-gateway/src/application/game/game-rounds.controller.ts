import {
  CreateGameRoundRequest,
  GameRoundsByPlayerAndMapResponse,
  RoundsByPlayerQuery,
} from '@app/api/game-api';
import {
  GameRoundsQuery,
  GameRoundsResponse,
} from '@app/api/game-api/dto/queries/rounds.query';
import { Body, Controller, Post } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiHideProperty, ApiResponse, ApiTags } from '@nestjs/swagger';

import { CreateGameRoundCommand } from './commands';
import { GameRoundsByPlayerAndMapQuery } from './queries';
import { RoundsReportService, SECONDS_IN_1_DAY } from './services';
import { RoundsReport } from './services/types';

@Controller('game')
@ApiTags('game')
export class GameRoundsController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly roundsReportService: RoundsReportService,
  ) {}

  @Post('create')
  @ApiResponse({
    status: 200,
    description: 'Created game-round entry successfully',
  })
  async createGameRound(
    @Body() payload: CreateGameRoundRequest,
  ): Promise<void> {
    return this.commandBus.execute<CreateGameRoundCommand, void>(
      new CreateGameRoundCommand(payload),
    );
  }

  @Post('create-report')
  @ApiResponse({
    status: 200,
    description: 'Created round-report successfully',
  })
  createGameRoundReport(@Body() query: GameRoundsQuery): Promise<RoundsReport> {
    const daysBack = query.daysBack || 7;
    const secondsBackInTime = Date.now() - SECONDS_IN_1_DAY * daysBack * 1000;
    query.date = secondsBackInTime.toString();

    return this.roundsReportService.generateReportFor(query);
  }

  @Post('rounds')
  @ApiResponse({
    status: 200,
    description: 'Fetched round-report successfully',
  })
  async getRounds(@Body() query: GameRoundsQuery): Promise<GameRoundsResponse> {
    return this.queryBus.execute<GameRoundsQuery, GameRoundsResponse>(
      new GameRoundsQuery().copy(query),
    );
  }

  @Post('for-player')
  @ApiHideProperty()
  async getRoundsForPlayer(
    @Body() query: RoundsByPlayerQuery,
  ): Promise<GameRoundsByPlayerAndMapResponse> {
    return this.queryBus.execute<
      GameRoundsByPlayerAndMapQuery,
      GameRoundsByPlayerAndMapResponse
    >(new GameRoundsByPlayerAndMapQuery(query));
  }
}
