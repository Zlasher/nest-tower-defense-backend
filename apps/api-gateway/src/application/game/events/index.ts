import { ChallengeActivatedHandler } from './handlers/challenge-activated.handler';
import { GameRoundCreatedHandler } from './handlers/game-rounds-created.handler';
import { ItemCodeClaimedHandler } from './handlers/item-code-claim.events';

export const GameRoundEventHandlers = [GameRoundCreatedHandler];
export const ItemCodeEventsHandlers = [ItemCodeClaimedHandler];
export const ChallengeEventHandlers = [ChallengeActivatedHandler];

export * from './impl/game-round-created.event';
export * from './impl/item-code-claimed.events';
export * from './impl/challenge-activated.event';
