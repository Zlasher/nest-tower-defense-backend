import { ItemCode } from 'apps/api-gateway/src/domain/game';

export class ItemCodeClaimedEvent {
  constructor(
    public readonly itemCode: ItemCode,
    public readonly playerId: string,
  ) {}
}
