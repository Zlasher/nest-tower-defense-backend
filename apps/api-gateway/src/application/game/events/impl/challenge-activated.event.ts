import { PlayerChallenge } from 'apps/api-gateway/src/domain/game';

export class ChallengeActivatedEvent {
  constructor(public readonly challenge: PlayerChallenge) {}
}
