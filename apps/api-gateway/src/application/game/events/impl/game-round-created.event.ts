import { GameRound } from 'apps/api-gateway/src/domain/game';

export class GameRoundCreatedEvent {
  constructor(public readonly gameRound: GameRound) {}
}
