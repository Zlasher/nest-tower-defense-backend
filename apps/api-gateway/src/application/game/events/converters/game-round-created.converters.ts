import { GameRound } from 'apps/api-gateway/src/domain/game';

import { ChannelType, CreateNotificationDTO } from '@app/api/notification-api';

import { GameRoundCreatedEvent } from '../impl/game-round-created.event';

const formatMapName = (mapId: string): string => {
  switch (mapId) {
    case 'LEVEL_CHALLENGE_ENDLESS_2':
      return 'Endless Hardcore level';
    case 'LEVEL_CHALLENGE_ENDLESS_3':
      return 'Elemental Endless level';
    case 'LEVEL_CHALLENGE_ENDLESS_4':
      return 'Unstable Rift Endless level';
    default:
      return 'Endless level';
  }
};

export const createNotificationForRegularLevel = ({
  gameRound,
}: GameRoundCreatedEvent): CreateNotificationDTO => {
  return new CreateNotificationDTO(
    gameRound.playerId,
    createMessageForRegularLevel(gameRound),
    '',
    ChannelType.Notification,
  );
};

export const createNotificationForEndlessLevel = ({
  gameRound,
}: GameRoundCreatedEvent): CreateNotificationDTO => {
  return new CreateNotificationDTO(
    gameRound.playerId,
    createMessageForArenaLevel(gameRound),
    '',
    ChannelType.Arena,
  );
};

const createMessageForRegularLevel = (gameRound: GameRound): string => {
  const state = gameRound.lives > 0 ? 'completed' : 'failed';

  return `PLAYER_NAME ${state} ${gameRound.mapId}\nLives: ${
    gameRound.lives
  } - Time: ${gameRound.seconds} - Tier: ${gameRound.tier}\nChallenge: ${
    gameRound.challengeId || 'N/A'
  }`;
};

const createMessageForArenaLevel = (gameRound: GameRound): string => {
  return `PLAYER_NAME played ${formatMapName(gameRound.mapId)} --> ${
    gameRound.wavesProgress.split('/')[0]
  } waves!`;
};
