import { MessageService } from 'apps/api-gateway/src/application/message/message.service';

import {
  CreateNotificationDTO,
  INotificationEvent,
} from '@app/api/notification-api';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';

import { PlayerService } from '../../../player';
import {
  createNotificationForEndlessLevel,
  createNotificationForRegularLevel,
} from '../converters/game-round-created.converters';
import { GameRoundCreatedEvent } from '../impl/game-round-created.event';
import { ChallengesService } from '../../services';

@EventsHandler(GameRoundCreatedEvent)
export class GameRoundCreatedHandler
  implements
    IEventHandler<GameRoundCreatedEvent>,
    INotificationEvent<GameRoundCreatedEvent>
{
  constructor(
    private readonly messageService: MessageService,
    private readonly playerService: PlayerService,
    private readonly challengeService: ChallengesService,
  ) {}

  async handle(gameRoundCreatedEvent: GameRoundCreatedEvent): Promise<void> {
    this.handleNotification(gameRoundCreatedEvent);
    this.handleCheckForChallenge(gameRoundCreatedEvent);
  }

  private async handleCheckForChallenge({
    gameRound,
  }: GameRoundCreatedEvent): Promise<void> {
    if (!gameRound.challengeId) {
      return;
    }

    this.challengeService.createRecord(gameRound);
  }

  private async handleNotification(
    gameRoundCreatedEvent: GameRoundCreatedEvent,
  ): Promise<void> {
    const notificationDto = this.createNotification(gameRoundCreatedEvent);

    const player = await this.playerService.getPlayerById(
      gameRoundCreatedEvent.gameRound,
    );

    const modifiedDto: CreateNotificationDTO = {
      ...notificationDto,
      message: notificationDto.message.replace(
        'PLAYER_NAME',
        player?.name || 'Unknown',
      ),
    };

    this.messageService.createNotification(modifiedDto);
  }

  public createNotification(
    gameRoundCreatedEvent: GameRoundCreatedEvent,
  ): CreateNotificationDTO {
    const isEndlessMap = gameRoundCreatedEvent.gameRound.mapId
      .toLocaleLowerCase()
      .includes('endless');

    if (isEndlessMap) {
      return createNotificationForEndlessLevel(gameRoundCreatedEvent);
    }

    return createNotificationForRegularLevel(gameRoundCreatedEvent);
  }
}
