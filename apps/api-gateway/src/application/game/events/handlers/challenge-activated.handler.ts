import {
  ChannelType,
  CreateNotificationDTO,
  INotificationEvent,
} from '@app/api/notification-api';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';

import { MessageService } from 'apps/api-gateway/src/application/message/message.service';
import { ChallengeActivatedEvent } from '../impl/challenge-activated.event';
import { PlayerService } from '../../../player';

@EventsHandler(ChallengeActivatedEvent)
export class ChallengeActivatedHandler
  implements
    IEventHandler<ChallengeActivatedEvent>,
    INotificationEvent<ChallengeActivatedEvent>
{
  constructor(
    private readonly messageService: MessageService,
    private readonly playerService: PlayerService,
  ) {}

  async handle(
    challengeActivatedEvent: ChallengeActivatedEvent,
  ): Promise<void> {
    this.handleNotification(challengeActivatedEvent);
  }

  private async handleNotification(
    challengeActivatedEvent: ChallengeActivatedEvent,
  ): Promise<void> {
    const notificationDto = this.createNotification(challengeActivatedEvent);

    const player = await this.playerService.getPlayerById(
      challengeActivatedEvent.challenge,
    );

    const modifiedDto: CreateNotificationDTO = {
      ...notificationDto,
      message: notificationDto.message.replace(
        'PLAYER_NAME',
        player?.name || 'Unknown',
      ),
    };

    this.messageService.createNotification(modifiedDto);
  }

  createNotification({
    challenge: { playerId, name, description },
  }: ChallengeActivatedEvent): CreateNotificationDTO {
    return new CreateNotificationDTO(
      playerId,
      `PLAYER_NAME just published a new Challenge!\n\n${name}\n${description}`,
      '',
      ChannelType.Purchases,
    );
  }
}
