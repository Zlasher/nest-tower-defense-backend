import {
  ChannelType,
  CreateNotificationDTO,
  INotificationEvent,
} from '@app/api/notification-api';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';

import { ItemCodeClaimedEvent } from '../impl/item-code-claimed.events';
import { MessageService } from 'apps/api-gateway/src/application/message/message.service';

@EventsHandler(ItemCodeClaimedEvent)
export class ItemCodeClaimedHandler
  implements
    IEventHandler<ItemCodeClaimedEvent>,
    INotificationEvent<ItemCodeClaimedEvent>
{
  constructor(private readonly messageService: MessageService) {}

  async handle(gameRoundCreatedEvent: ItemCodeClaimedEvent): Promise<void> {
    this.messageService.createNotification(
      this.createNotification(gameRoundCreatedEvent),
    );
  }

  createNotification({
    itemCode,
    playerId,
  }: ItemCodeClaimedEvent): CreateNotificationDTO {
    return new CreateNotificationDTO(
      playerId,
      `${playerId} claimed code: ${itemCode.getCode()}`,
      '',
      ChannelType.Notification,
    );
  }
}
