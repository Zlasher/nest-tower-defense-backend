import { MessageDtoRepository } from 'apps/api-gateway/src/infrastructure/player';

import { InboxMessageDTO, PlayerInboxResponse } from '@app/api/player-api';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';

import { InboxQuery } from '../impl/inbox.query';

@QueryHandler(InboxQuery)
export class InboxHandler implements IQueryHandler<InboxQuery> {
  constructor(private readonly messageDtoRepository: MessageDtoRepository) {}

  async execute({ playerId }: InboxQuery): Promise<PlayerInboxResponse> {
    const messages = await this.messageDtoRepository.find({
      playerId,
      isRead: false,
    });

    return {
      messages: messages.map((message) => new InboxMessageDTO(message)),
    };
  }
}
