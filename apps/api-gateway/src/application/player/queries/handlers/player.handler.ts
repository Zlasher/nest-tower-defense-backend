import { PlayerDtoRepository } from 'apps/api-gateway/src/infrastructure/player';

import { IPlayer, PlayerDTO } from '@app/api/player-api';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';

import { PlayerQuery } from '../impl/player.query';

@QueryHandler(PlayerQuery)
export class PlayersHandler implements IQueryHandler<PlayerQuery> {
  constructor(private readonly playerDtoRepository: PlayerDtoRepository) {}

  async execute({ playerId }: PlayerQuery): Promise<PlayerDTO> {
    return this.playerDtoRepository
      .findOne({
        playerId,
      })
      .then((player: IPlayer) => new PlayerDTO(player));
  }
}
