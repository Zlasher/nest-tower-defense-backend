import { ByPlayerIdRequest } from '@app/api/player-api';
import { IsNotEmpty, IsString } from 'class-validator';

export class PlayerQuery {
  @IsString()
  @IsNotEmpty()
  public readonly playerId: string;

  constructor(query: ByPlayerIdRequest) {
    this.playerId = query.playerId;
  }
}
