import { InboxHandler } from './handlers/inbox.handler';
import { PlayersHandler } from './handlers/player.handler';

export const PlayerQueryHandlers = [PlayersHandler];
export const InboxMessageQueryHandlers = [InboxHandler];

export * from './impl/player.query';
export * from './impl/inbox.query';
