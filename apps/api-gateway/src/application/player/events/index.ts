import { PlayerCreatedHandler } from './handlers/player-created.handler';
// import { PlayerLoggedInHandler } from './handlers/player-loggedin.handler';
import { PlayerPurchasedHandler } from './handlers/player-purchased.handler';
import { PlayerReCreatedHandler } from './handlers/player-recreated.handler';

export const PlayerEventHandlers = [
  PlayerCreatedHandler,
  PlayerReCreatedHandler,
  // PlayerLoggedInHandler,
  PlayerPurchasedHandler,
];

export * from './impl/player-created.event';
export * from './impl/player-recreated.event';
export * from './impl/player-loggedin.event';
export * from './impl/player-purchased.event';
