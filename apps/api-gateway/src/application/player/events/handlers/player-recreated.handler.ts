import {
  ChannelType,
  CreateNotificationDTO,
  INotificationEvent,
} from '@app/api/notification-api';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';

import { PlayerReCreatedEvent } from '../impl/player-recreated.event';
import { MessageService } from 'apps/api-gateway/src/application/message/message.service';

@EventsHandler(PlayerReCreatedEvent)
export class PlayerReCreatedHandler
  implements
    IEventHandler<PlayerReCreatedEvent>,
    INotificationEvent<PlayerReCreatedEvent>
{
  constructor(private readonly messageService: MessageService) {}

  async handle(playerReCreatedEvent: PlayerReCreatedEvent): Promise<void> {
    this.messageService.createNotification(
      this.createNotification(playerReCreatedEvent),
    );
  }

  createNotification({ player }: PlayerReCreatedEvent): CreateNotificationDTO {
    return new CreateNotificationDTO(
      player.playerId,
      `${player.getName()} (${player.getplayerId()}) signed up again`,
      player.gameVersion,
      ChannelType.Purchases,
    );
  }
}
