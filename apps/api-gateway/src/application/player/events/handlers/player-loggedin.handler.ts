import {
  ChannelType,
  CreateNotificationDTO,
  INotificationEvent,
} from '@app/api/notification-api';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';

import { PlayerLoggedInEvent } from '../impl/player-loggedin.event';
import { MessageService } from 'apps/api-gateway/src/application/message/message.service';

@EventsHandler(PlayerLoggedInEvent)
export class PlayerLoggedInHandler
  implements
    IEventHandler<PlayerLoggedInEvent>,
    INotificationEvent<PlayerLoggedInEvent>
{
  constructor(private readonly messageService: MessageService) {}

  async handle(playerLoggedInEvent: PlayerLoggedInEvent): Promise<void> {
    this.messageService.createNotification(
      this.createNotification(playerLoggedInEvent),
    );
  }

  createNotification({ player }: PlayerLoggedInEvent): CreateNotificationDTO {
    return new CreateNotificationDTO(
      player.playerId,
      `${player.getName()} (${player.getplayerId()}) logged in`,
      player.gameVersion,
      ChannelType.Notification,
    );
  }
}
