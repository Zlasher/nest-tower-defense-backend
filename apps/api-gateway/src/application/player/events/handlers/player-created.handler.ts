import {
  ChannelType,
  CreateNotificationDTO,
  INotificationEvent,
} from '@app/api/notification-api';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';

import { PlayerCreatedEvent } from '../impl/player-created.event';
import { MessageService } from 'apps/api-gateway/src/application/message/message.service';

@EventsHandler(PlayerCreatedEvent)
export class PlayerCreatedHandler
  implements
    IEventHandler<PlayerCreatedEvent>,
    INotificationEvent<PlayerCreatedEvent>
{
  constructor(private readonly messageService: MessageService) {}

  async handle(playerCreatedEvent: PlayerCreatedEvent): Promise<void> {
    this.messageService.createNotification(
      this.createNotification(playerCreatedEvent),
    );
  }

  createNotification({ player }: PlayerCreatedEvent): CreateNotificationDTO {
    return new CreateNotificationDTO(
      player.playerId,
      `${player.getName()} (${player.getplayerId()}) signed up`,
      player.gameVersion,
      ChannelType.Notification,
    );
  }
}
