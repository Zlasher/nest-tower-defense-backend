import { PurchaseFactory } from 'apps/api-gateway/src/domain/player';

import {
  ChannelType,
  CreateNotificationDTO,
  INotificationEvent,
} from '@app/api/notification-api';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';

import { PlayerPurchasedEvent } from '../impl/player-purchased.event';
import { MessageService } from '../../../message/message.service';

@EventsHandler(PlayerPurchasedEvent)
export class PlayerPurchasedHandler
  implements
    IEventHandler<PlayerPurchasedEvent>,
    INotificationEvent<PlayerPurchasedEvent>
{
  constructor(
    private readonly messageService: MessageService,
    private readonly purchaseFactory: PurchaseFactory,
  ) {}

  async handle(playerPurchasedEvent: PlayerPurchasedEvent): Promise<void> {
    this.purchaseFactory.create(playerPurchasedEvent);

    this.messageService.createNotification(
      this.createNotification(playerPurchasedEvent),
    );
  }

  createNotification(
    playerPurchasedEvent: PlayerPurchasedEvent,
  ): CreateNotificationDTO {
    const { player, itemId } = playerPurchasedEvent;
    return new CreateNotificationDTO(
      player.playerId,
      `${player.getName()} (${player.getplayerId()}) purchased ${itemId}`,
      player.gameVersion,
      ChannelType.Notification,
    );
  }
}
