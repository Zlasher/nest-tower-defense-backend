import { Player } from 'apps/api-gateway/src/domain/player';

export class PlayerLoggedInEvent {
  constructor(public readonly player: Player) {}
}
