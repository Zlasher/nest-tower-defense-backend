import { Player } from 'apps/api-gateway/src/domain/player';

export class PlayerCreatedEvent {
  constructor(public readonly player: Player) {}
}
