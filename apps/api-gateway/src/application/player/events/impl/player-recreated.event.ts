import { Player } from 'apps/api-gateway/src/domain/player';

export class PlayerReCreatedEvent {
  constructor(public readonly player: Player) {}
}
