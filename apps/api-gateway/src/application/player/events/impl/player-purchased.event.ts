import { Player } from 'apps/api-gateway/src/domain/player';

export class PlayerPurchasedEvent {
  constructor(public readonly player: Player, public readonly itemId: string) {}
}
