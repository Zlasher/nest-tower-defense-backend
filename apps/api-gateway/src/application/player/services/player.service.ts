import { ByPlayerIdRequest, PlayerDTO } from '@app/api/player-api';
import { PlayerCacheService } from '@app/common/modules/cache/player-cache.service';
import { Injectable } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';

import { PlayerQuery } from '../queries';

@Injectable()
export class PlayerService {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly playerCacheService: PlayerCacheService,
  ) {}

  public async getPlayerById(query: ByPlayerIdRequest): Promise<PlayerDTO> {
    const cachedPlayer = await this.playerCacheService.getById({
      playerId: query.playerId,
    });

    if (cachedPlayer) {
      return cachedPlayer;
    }

    return this.queryBus
      .execute<PlayerQuery, PlayerDTO>(new PlayerQuery(query))
      .then((player) => {
        this.playerCacheService.setById(player);

        return player;
      })
      .catch(() => null);
  }
}
