import {
  ByPlayerIdRequest,
  CreateInboxMessageRequest,
  PlayerInboxResponse,
  UpdateInboxMessageRequest,
} from '@app/api/player-api';
import { Body, Controller, Post } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import {
  CreateInboxMessageCommand,
  InboxQuery,
  UpdateInboxMessageCommand,
} from './';

@Controller('inbox')
@ApiTags('player')
export class InboxController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post()
  @ApiResponse({
    status: 200,
    description: 'Inbox fetched successfully',
    type: PlayerInboxResponse,
  })
  async getInbox(
    @Body() query: ByPlayerIdRequest,
  ): Promise<PlayerInboxResponse> {
    return this.queryBus.execute<InboxQuery, PlayerInboxResponse>(
      new InboxQuery(query),
    );
  }

  @Post('create')
  @ApiResponse({
    status: 200,
    description: 'Inbox message created successfully',
  })
  async createInboxMessage(
    @Body() payload: CreateInboxMessageRequest,
  ): Promise<void> {
    return this.commandBus.execute<CreateInboxMessageCommand, void>(
      new CreateInboxMessageCommand(payload),
    );
  }

  @Post('set-read')
  @ApiResponse({
    status: 200,
    description: 'Inbox message updated successfully',
  })
  async updateInboxMessage(
    @Body() payload: UpdateInboxMessageRequest,
  ): Promise<void> {
    return this.commandBus.execute<UpdateInboxMessageCommand, void>(
      new UpdateInboxMessageCommand(payload),
    );
  }
}
