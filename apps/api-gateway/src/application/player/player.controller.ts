import {
  ByPlayerIdRequest,
  CreatePlayerRequest,
  IsPlayerPremiumResponse,
  PlayerDTO,
  PurchasePlayerRequest,
} from 'libs/api/src/player-api';

import { Body, Controller, Post } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { LoginPlayerCommand, PromotePlayerCommand } from './commands';
import { CreatePlayerCommand } from './commands/impl/create-player.command';
import { PurchasePlayerCommand } from './commands/impl/purchase-player.command';
import { PlayerQuery } from './queries';
import { PlayerService } from './services';

@Controller('player')
@ApiTags('player')
export class PlayerController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly playerService: PlayerService,
  ) {}

  @Post()
  @ApiResponse({
    status: 200,
    description: 'Fetched player successfully',
    type: PlayerDTO,
  })
  getPlayerById(@Body() query: ByPlayerIdRequest): Promise<PlayerDTO> {
    return this.playerService.getPlayerById(query);
  }

  @Post('isPremium')
  @ApiResponse({
    status: 200,
    description: 'Fetched player isPremium successfully',
    type: IsPlayerPremiumResponse,
  })
  isPlayerPremium(
    @Body() query: ByPlayerIdRequest,
  ): Promise<IsPlayerPremiumResponse> {
    return this.queryBus.execute<PlayerQuery, PlayerDTO>(
      new PlayerQuery(query),
    );
  }

  @Post('login')
  @ApiResponse({
    status: 200,
    description: 'Logged in player successfully',
  })
  onPlayerLogin(@Body() payload: ByPlayerIdRequest): Promise<void> {
    return this.commandBus.execute<LoginPlayerCommand, void>(
      new LoginPlayerCommand(payload),
    );
  }

  @Post('signup')
  @ApiResponse({
    status: 200,
    description: 'Created player successfully',
  })
  onPlayerSignup(@Body() payload: CreatePlayerRequest): Promise<void> {
    return this.commandBus.execute<CreatePlayerCommand, void>(
      new CreatePlayerCommand(payload),
    );
  }

  @Post('purchase')
  @ApiResponse({
    status: 200,
    description: 'Recorded purchase successfully',
  })
  onItemPurchased(@Body() payload: PurchasePlayerRequest): Promise<void> {
    return this.commandBus.execute<PurchasePlayerCommand, void>(
      new PurchasePlayerCommand(payload),
    );
  }

  @Post('promote')
  @ApiResponse({
    status: 200,
    description: 'Promoted player successfully',
  })
  onPlayerPromote(@Body() payload: ByPlayerIdRequest) {
    return this.commandBus.execute<PromotePlayerCommand, void>(
      new PromotePlayerCommand(payload),
    );
  }
}
