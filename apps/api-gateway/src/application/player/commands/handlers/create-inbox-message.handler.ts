import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { CreateInboxMessageCommand } from '../impl/create-inbox-message.command';
import { InboxMessageFactory } from 'apps/api-gateway/src/domain/player/inbox-message.factory';

@CommandHandler(CreateInboxMessageCommand)
export class CreateInboxMessageHandler
  implements ICommandHandler<CreateInboxMessageCommand>
{
  constructor(
    private readonly messageFactory: InboxMessageFactory,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({
    createInboxMessageRequest,
  }: CreateInboxMessageCommand): Promise<void> {
    const messageEntity = this.eventPublisher.mergeObjectContext(
      await this.messageFactory.create(createInboxMessageRequest),
    );

    messageEntity.commit();
  }
}
