import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { CreatePlayerCommand } from '../impl/create-player.command';
import { PlayerFactory } from 'apps/api-gateway/src/domain/player/player.factory';
import { PlayerEntityRepository } from 'apps/api-gateway/src/infrastructure/player/repositories/player-entity.repository';

@CommandHandler(CreatePlayerCommand)
export class CreatePlayerHandler
  implements ICommandHandler<CreatePlayerCommand>
{
  constructor(
    private readonly playerEntityRepository: PlayerEntityRepository,
    private readonly playerFactory: PlayerFactory,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({ createPlayerRequest }: CreatePlayerCommand): Promise<void> {
    const existingPlayerDocument = await this.playerEntityRepository.findOneBy({
      playerId: createPlayerRequest.playerId,
    });

    if (existingPlayerDocument) {
      const existingPlayerEntity = this.eventPublisher.mergeObjectContext(
        existingPlayerDocument,
      );

      existingPlayerEntity.handleResignup(createPlayerRequest);

      await this.playerEntityRepository.findOneAndReplaceBy(
        { playerId: existingPlayerDocument.playerId },
        existingPlayerEntity,
      );

      existingPlayerEntity.commit();
      return;
    }

    const playerEntity = this.eventPublisher.mergeObjectContext(
      await this.playerFactory.create(createPlayerRequest),
    );

    playerEntity.commit();
  }
}
