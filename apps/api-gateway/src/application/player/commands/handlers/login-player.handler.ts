import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { LoginPlayerCommand } from '../impl/login-player.command';
import { PlayerFactory } from 'apps/api-gateway/src/domain/player/player.factory';
import { PlayerEntityRepository } from 'apps/api-gateway/src/infrastructure/player/repositories/player-entity.repository';

@CommandHandler(LoginPlayerCommand)
export class LoginPlayerHandler implements ICommandHandler<LoginPlayerCommand> {
  constructor(
    private readonly playerEntityRepository: PlayerEntityRepository,
    private readonly playerFactory: PlayerFactory,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({ loginPlayerRequest }: LoginPlayerCommand): Promise<void> {
    const existingPlayerDocument = await this.playerEntityRepository.findOneBy({
      playerId: loginPlayerRequest.playerId,
    });

    if (!existingPlayerDocument) {
      const playerEntity = this.eventPublisher.mergeObjectContext(
        await this.playerFactory.createEmpty(loginPlayerRequest.playerId),
      );

      playerEntity.commit();
      return;
    }

    const playerEntity = this.eventPublisher.mergeObjectContext(
      existingPlayerDocument,
    );

    playerEntity.handleLogin();

    await this.playerEntityRepository.findOneAndReplaceBy(
      { playerId: loginPlayerRequest.playerId },
      playerEntity,
    );

    playerEntity.commit();
  }
}
