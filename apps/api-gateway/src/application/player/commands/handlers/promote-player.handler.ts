import { Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { PromotePlayerCommand } from '../impl/promote-player.command';
import { PlayerEntityRepository } from 'apps/api-gateway/src/infrastructure/player/repositories/player-entity.repository';

@CommandHandler(PromotePlayerCommand)
export class PromotePlayerHandler
  implements ICommandHandler<PromotePlayerCommand>
{
  protected readonly logger = new Logger(PromotePlayerHandler.name);

  constructor(
    private readonly playerEntityRepository: PlayerEntityRepository,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({ promotePlayerRequest }: PromotePlayerCommand): Promise<void> {
    const existingDocument = await this.playerEntityRepository.findOneBy({
      playerId: promotePlayerRequest.playerId,
    });

    if (!existingDocument) {
      this.logger.warn(
        `No player was found with id ${promotePlayerRequest.playerId}`,
      );
      return;
    }

    const playerEntity =
      this.eventPublisher.mergeObjectContext(existingDocument);

    playerEntity.isPremium = true;

    await this.playerEntityRepository.findOneAndReplaceBy(
      { playerId: promotePlayerRequest.playerId },
      playerEntity,
    );

    playerEntity.commit();
  }
}
