import { Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { UpdateInboxMessageCommand } from '../impl/update-inbox-message.command';
import { InboxMessageEntityRepository } from 'apps/api-gateway/src/infrastructure/player/repositories/message-entity.repository';

@CommandHandler(UpdateInboxMessageCommand)
export class UpdateInboxMessageHandler
  implements ICommandHandler<UpdateInboxMessageCommand>
{
  protected readonly logger = new Logger(UpdateInboxMessageHandler.name);

  constructor(
    private readonly messageEntityRepository: InboxMessageEntityRepository,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({
    updateInboxMessageRequest,
  }: UpdateInboxMessageCommand): Promise<void> {
    const existingDocument = await this.messageEntityRepository.findOneBy({
      _id: updateInboxMessageRequest.messageId,
      isRead: false,
    });

    if (!existingDocument) {
      this.logger.warn(
        `Player tried to update non-existent message with id ${updateInboxMessageRequest.messageId}`,
      );
      return;
    }
    const messageEntity =
      this.eventPublisher.mergeObjectContext(existingDocument);

    messageEntity.handleSetRead();

    await this.messageEntityRepository.findOneAndReplaceBy(
      { _id: updateInboxMessageRequest.messageId },
      messageEntity,
    );

    messageEntity.commit();
  }
}
