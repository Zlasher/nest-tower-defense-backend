import { Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { PurchasePlayerCommand } from '../impl/purchase-player.command';
import { PlayerEntityRepository } from 'apps/api-gateway/src/infrastructure/player/repositories/player-entity.repository';

@CommandHandler(PurchasePlayerCommand)
export class PurchasePlayerHandler
  implements ICommandHandler<PurchasePlayerCommand>
{
  protected readonly logger = new Logger(PurchasePlayerHandler.name);

  constructor(
    private readonly playerEntityRepository: PlayerEntityRepository,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute({
    purchasePlayerRequest,
  }: PurchasePlayerCommand): Promise<void> {
    const existingDocument = await this.playerEntityRepository.findOneBy({
      playerId: purchasePlayerRequest.playerId,
    });

    if (!existingDocument) {
      this.logger.warn(
        `No player was found with id ${purchasePlayerRequest.playerId}`,
      );
      return;
    }

    const playerEntity =
      this.eventPublisher.mergeObjectContext(existingDocument);

    playerEntity.handlePurchasedItem(purchasePlayerRequest);

    await this.playerEntityRepository.findOneAndReplaceBy(
      { playerId: purchasePlayerRequest.playerId },
      playerEntity,
    );

    playerEntity.commit();
  }
}
