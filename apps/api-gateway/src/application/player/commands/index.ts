import { CreateInboxMessageHandler } from './handlers/create-inbox-message.handler';
import { CreatePlayerHandler } from './handlers/create-player.handler';
import { LoginPlayerHandler } from './handlers/login-player.handler';
import { PurchasePlayerHandler } from './handlers/purchase-player.handler';
import { UpdateInboxMessageHandler } from './handlers/update-inbox-message.handler';
import { PromotePlayerHandler } from './handlers/promote-player.handler';

export const PlayerCommandHandlers = [
  CreatePlayerHandler,
  PurchasePlayerHandler,
  LoginPlayerHandler,
  CreateInboxMessageHandler,
  UpdateInboxMessageHandler,
  PromotePlayerHandler,
];

export * from './impl/create-player.command';
export * from './impl/purchase-player.command';
export * from './impl/login-player.command';
export * from './impl/create-inbox-message.command';
export * from './impl/update-inbox-message.command';
export * from './impl/promote-player.command';
