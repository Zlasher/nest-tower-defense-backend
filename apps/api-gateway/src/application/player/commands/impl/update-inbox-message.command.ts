import { UpdateInboxMessageRequest } from '@app/api/player-api';

export class UpdateInboxMessageCommand {
  constructor(
    public readonly updateInboxMessageRequest: UpdateInboxMessageRequest,
  ) {}
}
