import { ByPlayerIdRequest } from '@app/api/player-api';

export class LoginPlayerCommand {
  constructor(public readonly loginPlayerRequest: ByPlayerIdRequest) {}
}
