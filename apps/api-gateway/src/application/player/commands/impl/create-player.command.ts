import { CreatePlayerRequest } from '@app/api/player-api';

export class CreatePlayerCommand {
  constructor(public readonly createPlayerRequest: CreatePlayerRequest) {}
}
