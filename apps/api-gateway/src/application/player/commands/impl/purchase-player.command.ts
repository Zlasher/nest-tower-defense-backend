import { PurchasePlayerRequest } from '@app/api/player-api';

export class PurchasePlayerCommand {
  constructor(public readonly purchasePlayerRequest: PurchasePlayerRequest) {}
}
