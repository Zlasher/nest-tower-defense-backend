import { CreateInboxMessageRequest } from '@app/api/player-api';

export class CreateInboxMessageCommand {
  constructor(
    public readonly createInboxMessageRequest: CreateInboxMessageRequest,
  ) {}
}
