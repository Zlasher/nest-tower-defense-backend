import { ByPlayerIdRequest } from '@app/api/player-api';

export class PromotePlayerCommand {
  constructor(public readonly promotePlayerRequest: ByPlayerIdRequest) {}
}
