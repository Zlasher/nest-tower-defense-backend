import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export default function addSwagger(app: INestApplication): void {
  const config = new DocumentBuilder()
    .setTitle('Game Backend')
    .setDescription('Backend for Maze Defenders')
    .setVersion('1.1')
    .addTag('game-api')
    .build();

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('api', app, document);
}
