import { rateLimit } from 'express-rate-limit';
import helmet from 'helmet';

import { appVersion } from '@app/common';
import { HttpExceptionFilter } from '@app/common/exception-filters';
import { AuthGuard } from '@app/common/guards';
import { ValidationPipe } from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';

import { ApiGatewayModule } from './api-gateway.module';
import addSwagger from './swagger';

async function bootstrap() {
  const app = await NestFactory.create(ApiGatewayModule, {
    snapshot: true,
    cors: true,
  });

  app.use(helmet());

  app.use(
    rateLimit({
      windowMs: 10 * 60 * 1000, // 10 minutes
      max: 200, // Limit each IP to 200 requests per `window` (here, per 10 minutes)
      standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
      legacyHeaders: false, // Disable the `X-RateLimit-*` headers,
      skipSuccessfulRequests: false, // The counting will skip all successful requests and just count the errors. Instead of removing rate-limiting, it's better to set this to true to limit the number of times a request fails. Can help prevent against brute-force attacks
      message: {
        message: 'Too many requests! Come back later',
        statusCode: 403,
      },
    }),
  );

  const reflector = app.get(Reflector);

  app.useGlobalGuards(new AuthGuard(reflector));
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new HttpExceptionFilter());

  if (process.env.NODE_ENV !== 'production') {
    addSwagger(app);
  }

  const port = process.env.PORT || 3000;
  await app.listen(port);

  console.log(`🚀 API-service (v${appVersion}) up and running on port`, port);
}

bootstrap();
