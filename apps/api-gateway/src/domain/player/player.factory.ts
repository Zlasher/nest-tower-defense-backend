import { CreatePlayerRequest } from '@app/api/player-api';
import { EntityFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { PlayerCreatedEvent } from '../../application/player';
import { Player } from './entities/Player';
import { PlayerEntityRepository } from '../../infrastructure/player/repositories/player-entity.repository';

@Injectable()
export class PlayerFactory implements EntityFactory<Player> {
  constructor(
    private readonly playerEntityRepository: PlayerEntityRepository,
  ) {}

  async savePlayer(player: Player): Promise<Player> {
    await this.playerEntityRepository.create(player);
    player.apply(new PlayerCreatedEvent(player));

    return player;
  }

  async create(createPlayerRequest: CreatePlayerRequest): Promise<Player> {
    const currentDate = Date.now().toString();

    const player = new Player({
      ...createPlayerRequest,
      createdDate: currentDate,
      lastLogin: currentDate,
      isPremium: false,
      discordId: undefined,
    });

    await this.savePlayer(player);
    return player;
  }

  async createEmpty(playerId: string): Promise<Player> {
    const currentDate = Date.now().toString();

    const player = new Player({
      playerId,
      name: 'Unknown',
      device: 'Unknown',
      gameVersion: '2.2.04',
      createdDate: currentDate,
      lastLogin: currentDate,
      isPremium: false,
      discordId: undefined,
    });

    await this.savePlayer(player);
    return player;
  }
}
