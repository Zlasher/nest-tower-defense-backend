import { CreateInboxMessageRequest } from '@app/api/player-api';
import { Injectable } from '@nestjs/common';

import { MessageDtoRepository } from '../../infrastructure/player';
import { InboxMessage } from './entities/InboxMessage';

@Injectable()
export class InboxMessageFactory {
  constructor(private readonly messageDtoRepository: MessageDtoRepository) {}

  async create(
    createInboxMessageRequest: CreateInboxMessageRequest,
  ): Promise<InboxMessage> {
    const currentDate = Date.now().toString();

    const message = new InboxMessage({
      playerId: createInboxMessageRequest.playerId,
      message: createInboxMessageRequest.message,
      isRead: false,
      date: currentDate,
    });

    await this.messageDtoRepository.create(message);

    return message;
  }
}
