import { Injectable } from '@nestjs/common';

import { PlayerPurchasedEvent } from '../../application/player';
import {
  PurchaseDtoRepository,
  PurchaseSchema,
} from '../../infrastructure/player';

@Injectable()
export class PurchaseFactory {
  constructor(private readonly purchaseDtoRepository: PurchaseDtoRepository) {}

  async create(
    playerPurchasedEvent: PlayerPurchasedEvent,
  ): Promise<PurchaseSchema> {
    const currentDate = Date.now().toString();

    const document = await this.purchaseDtoRepository.create({
      playerId: playerPurchasedEvent.player.getplayerId(),
      itemId: playerPurchasedEvent.itemId,
      date: currentDate,
    });

    return document;
  }
}
