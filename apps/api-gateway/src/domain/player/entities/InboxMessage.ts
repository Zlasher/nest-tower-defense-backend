import { IMessageInbox } from '@app/api/player-api';
import { AggregateRoot } from '@nestjs/cqrs';

export class InboxMessage extends AggregateRoot implements IMessageInbox {
  readonly _id: any;
  readonly playerId: string;
  readonly message: string;
  readonly date: string;
  isRead: boolean;

  constructor(player: IMessageInbox) {
    super();

    this._id = player._id;
    this.playerId = player.playerId;
    this.message = player.message;
    this.date = new Date(Number(player.date)).toUTCString();
    this.isRead = player.isRead;
  }

  public getplayerId(): string {
    return this.playerId;
  }

  public getMessage(): string {
    return this.message;
  }

  public getDate(): string {
    return this.date;
  }

  public getIsRead(): boolean {
    return this.isRead;
  }

  public handleSetRead(): void {
    this.isRead = true;
  }
}
