import {
  CreatePlayerRequest,
  IPlayer,
  PurchasePlayerRequest,
} from '@app/api/player-api';
import { AggregateRoot } from '@nestjs/cqrs';

import {
  PlayerLoggedInEvent,
  PlayerPurchasedEvent,
  PlayerReCreatedEvent,
} from '../../../application/player';

export class Player extends AggregateRoot implements IPlayer {
  readonly _id: any;
  readonly playerId: string;
  readonly createdDate: string;

  name: string;
  device: string;
  gameVersion: string;
  lastLogin: string;
  isPremium: boolean;
  discordId: string;

  constructor(player: IPlayer) {
    super();

    this._id = player._id;
    this.playerId = player.playerId;
    this.name = player.name;
    this.device = player.device;
    this.createdDate = player.createdDate;
    this.lastLogin = player.lastLogin;
    this.gameVersion = player.gameVersion;
    this.isPremium = player.isPremium;
    this.discordId = player.discordId;
  }

  public getplayerId(): string {
    return this.playerId;
  }

  public getName(): string {
    return this.name;
  }

  public getDevice(): string {
    return this.name;
  }

  public getCreatedDate(): string {
    return this.createdDate;
  }

  public getLastLogin(): string {
    return this.lastLogin;
  }

  public getGameVersion(): string {
    return this.gameVersion;
  }

  public getDiscordId(): string {
    return this.discordId;
  }

  public getIsPremium(): boolean {
    return this.isPremium;
  }

  public handleLogin(): void {
    this.lastLogin = Date.now().toString();

    this.apply(new PlayerLoggedInEvent(this));
  }

  public handlePurchasedItem(
    purchasePlayerRequest: PurchasePlayerRequest,
  ): void {
    this.isPremium = true;

    this.apply(new PlayerPurchasedEvent(this, purchasePlayerRequest.itemId));
  }

  public handleResignup(createPlayerRequest: CreatePlayerRequest): void {
    this.name = createPlayerRequest.name;
    this.device = createPlayerRequest.device;
    this.gameVersion = createPlayerRequest.gameVersion;
    this.lastLogin = Date.now().toString();

    this.apply(new PlayerReCreatedEvent(this));
  }
}
