import { CreateGameRoundRequest } from '@app/api/game-api';
import { EntityFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { GameRoundCreatedEvent } from '../../application/game';
import { GameRoundEntityRepository } from '../../infrastructure/game';
import { GameRound } from './entities/GameRound';

@Injectable()
export class GameRoundFactory implements EntityFactory<GameRound> {
  constructor(
    private readonly gameRoundEntityRepository: GameRoundEntityRepository,
  ) {}

  async create(
    createGameRoundRequest: CreateGameRoundRequest,
  ): Promise<GameRound> {
    const entity = new GameRound({
      ...createGameRoundRequest,
      date: Date.now().toString(),
    });

    await this.gameRoundEntityRepository.create(entity);
    entity.apply(new GameRoundCreatedEvent(entity));

    return entity;
  }
}
