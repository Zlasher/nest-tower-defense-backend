import { CreateMapMessageRequest } from '@app/api/game-api';
import { EntityFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { MapMessageEntityRepository } from '../../infrastructure/game';
import { MapMessage } from './entities/MapMessage';

@Injectable()
export class MapMessageFactory implements EntityFactory<MapMessage> {
  constructor(
    private readonly mapMessageEntityRepository: MapMessageEntityRepository,
  ) {}

  async create(
    saveChallengeRequest: CreateMapMessageRequest,
  ): Promise<MapMessage> {
    const createdAt = Date.now().toString();

    const entity = new MapMessage({
      ...saveChallengeRequest,
      createdAt,
    });

    await this.mapMessageEntityRepository.create(entity);

    return entity;
  }
}
