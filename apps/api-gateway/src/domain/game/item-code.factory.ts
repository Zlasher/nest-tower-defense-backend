import { CreateItemCodesRequest } from '@app/api/game-api';
import { EntityFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { ItemCodeEntityRepository } from '../../infrastructure/game';
import { ItemCode } from './entities/ItemCode';

@Injectable()
export class ItemCodeFactory implements EntityFactory<ItemCode> {
  constructor(
    private readonly itemCodeEntityRepository: ItemCodeEntityRepository,
  ) {}

  async create(
    createGameRoundRequest: CreateItemCodesRequest,
  ): Promise<ItemCode[]> {
    const codes = createGameRoundRequest.codeInput.split(',');

    const entitysCreated: ItemCode[] = [];

    for (const code of codes) {
      const entity = new ItemCode({
        code: code.toLowerCase(),
        itemName: createGameRoundRequest.itemName,
        isUsed: false,
      });

      entitysCreated.push(entity);

      await this.itemCodeEntityRepository.create(entity);
    }

    return entitysCreated;
  }
}
