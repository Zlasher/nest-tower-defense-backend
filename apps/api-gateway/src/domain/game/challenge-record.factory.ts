import { CreateChallengeRecordRequest } from '@app/api/game-api';
import { EntityFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { ChallengeRecordEntityRepository } from '../../infrastructure/game';
import { ChallengeRecord } from './entities/ChallengeRecord';

@Injectable()
export class ChallengeRecordFactory implements EntityFactory<ChallengeRecord> {
  constructor(
    private readonly challengeRecordEntityRepository: ChallengeRecordEntityRepository,
  ) {}

  async create(
    request: CreateChallengeRecordRequest,
  ): Promise<ChallengeRecord> {
    const createdDate = Date.now().toString();

    const entity = new ChallengeRecord({
      ...request,
      createdDate,
    });

    await this.challengeRecordEntityRepository.create(entity);

    return entity;
  }
}
