import {
  IChallengeLeaderboard,
  ILeaderboardEntry,
} from '@app/api/game-api/interfaces/IChallengeLeaderboard';
import { AggregateRoot } from '@nestjs/cqrs';

export class ChallengeLeaderboard
  extends AggregateRoot
  implements IChallengeLeaderboard
{
  readonly _id: any;
  readonly challengeId: string;

  entries: ILeaderboardEntry[];

  constructor(leaderboard: IChallengeLeaderboard) {
    super();

    this._id = leaderboard._id;
    this.challengeId = leaderboard.challengeId;
    this.entries = leaderboard.entries;
  }

  public setEntries(entries: ILeaderboardEntry[]): void {
    this.entries = entries;
  }
}
