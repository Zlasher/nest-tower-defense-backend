import { IChallengeRecord } from '@app/api/game-api/interfaces/IChallengeRecord';
import { AggregateRoot } from '@nestjs/cqrs';

export class ChallengeRecord extends AggregateRoot implements IChallengeRecord {
  readonly _id: any;
  readonly challengeId: any;

  readonly playerId: string;
  readonly seconds: number;
  readonly createdDate: string;

  constructor(challege: IChallengeRecord) {
    super();

    this._id = challege._id;
    this.challengeId = challege.challengeId;
    this.playerId = challege.playerId;
    this.createdDate = challege.createdDate;
    this.seconds = challege.seconds;
  }
}
