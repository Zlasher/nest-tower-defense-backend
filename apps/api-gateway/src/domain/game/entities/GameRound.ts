import { IGame } from '@app/api/game-api';
import { AggregateRoot } from '@nestjs/cqrs';

export class GameRound extends AggregateRoot implements IGame {
  readonly _id: any;

  readonly playerId: string;
  readonly mapId: string;
  readonly wavesProgress: string;
  readonly lives: number;
  readonly seconds: number;
  readonly tier: number;
  readonly date: string;
  readonly challengeId?: string;

  constructor(game: IGame) {
    super();

    this._id = game._id;
    this.playerId = game.playerId;
    this.mapId = game.mapId;
    this.wavesProgress = game.wavesProgress;
    this.lives = game.lives;
    this.seconds = game.seconds;
    this.tier = game.tier;
    this.date = game.date;
    this.challengeId = game.challengeId;
  }

  public getplayerId(): string {
    return this.playerId;
  }

  public getMapId(): string {
    return this.mapId;
  }
}
