import { IChallenge, SaveChallengeRequest } from '@app/api/game-api';
import { AggregateRoot } from '@nestjs/cqrs';
import { ChallengeActivatedEvent } from 'apps/api-gateway/src/application/game';

export class PlayerChallenge extends AggregateRoot implements IChallenge {
  readonly _id: any;

  readonly playerId: string;
  mapId: string;
  readonly name: string;
  readonly description: string;
  readonly createdDate: string;
  modifiers: string[];
  excludedTowers?: string[];
  allowedTowers?: string[];

  isPendingCompletion: boolean;

  constructor(challege: IChallenge) {
    super();

    this._id = challege._id;
    this.playerId = challege.playerId;
    this.mapId = challege.mapId;
    this.name = challege.name;
    this.description = challege.description;
    this.modifiers = challege.modifiers;
    this.isPendingCompletion = challege.isPendingCompletion;
    this.createdDate = challege.createdDate;
    this.excludedTowers = challege.excludedTowers;
    this.allowedTowers = challege.allowedTowers;
  }

  public setActive(): void {
    this.isPendingCompletion = false;

    this.apply(new ChallengeActivatedEvent(this));
  }

  public update(delta: SaveChallengeRequest): void {
    this.mapId = delta.mapId;

    this.allowedTowers = delta.allowedTowers;
    this.excludedTowers = delta.excludedTowers;
    this.modifiers = delta.modifiers;
  }
}
