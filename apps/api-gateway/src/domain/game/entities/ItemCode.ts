import { IItemCode } from '@app/api/game-api';
import { AggregateRoot } from '@nestjs/cqrs';

import { ItemCodeClaimedEvent } from '../../../application/game';

const PERSISTENT_CODE = 'premium-md';

export class ItemCode extends AggregateRoot implements IItemCode {
  _id: any;
  code: string;
  itemName: string;
  isUsed: boolean;

  constructor(itemCode: IItemCode) {
    super();

    this._id = itemCode._id;
    this.code = itemCode.code;
    this.itemName = itemCode.itemName;
    this.isUsed = itemCode.isUsed;
  }

  public getCode(): string {
    return this.code;
  }

  public getItemName(): string {
    return this.itemName;
  }

  public getIsUsed(): boolean {
    return this.isUsed;
  }

  public handleClaim(playerId: string): void {
    this.isUsed = this.code !== PERSISTENT_CODE;

    this.apply(new ItemCodeClaimedEvent(this, playerId));
  }
}
