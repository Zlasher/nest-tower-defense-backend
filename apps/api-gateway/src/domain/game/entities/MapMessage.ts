import { IMapMessage } from '@app/api/game-api';
import { AggregateRoot } from '@nestjs/cqrs';

export class MapMessage extends AggregateRoot implements IMapMessage {
  readonly _id: any;

  readonly mapId: string;
  readonly message: string;
  readonly position: string;
  readonly createdBy: string;
  readonly createdAt: string;

  constructor(mapMessage: IMapMessage) {
    super();

    this._id = mapMessage._id;
    this.mapId = mapMessage.mapId;
    this.message = mapMessage.message;
    this.position = mapMessage.position;
    this.createdBy = mapMessage.createdBy;
    this.createdAt = mapMessage.createdAt;
  }
}
