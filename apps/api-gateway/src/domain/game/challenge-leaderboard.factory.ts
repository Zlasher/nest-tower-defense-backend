import { UpdateChallengeLeaderboardRequest } from '@app/api/game-api';
import { EntityFactory } from '@app/common/modules';
import { Injectable, Logger } from '@nestjs/common';

import { ChallengeLeaderboardEntityRepository } from '../../infrastructure/game/repositories/challenge-leaderboard-entity.repository';
import { ChallengeLeaderboard } from './entities/ChallengeLeaderboard';

@Injectable()
export class ChallengeLeaderboardFactory
  implements EntityFactory<ChallengeLeaderboard>
{
  protected readonly logger = new Logger(ChallengeLeaderboardFactory.name);

  constructor(
    private readonly challengeLeaderboardEntityRepository: ChallengeLeaderboardEntityRepository,
  ) {}

  async create(
    request: UpdateChallengeLeaderboardRequest,
  ): Promise<ChallengeLeaderboard> {
    const entity = new ChallengeLeaderboard({
      ...request,
    });

    await this.challengeLeaderboardEntityRepository.create(entity);

    return entity;
  }
}
