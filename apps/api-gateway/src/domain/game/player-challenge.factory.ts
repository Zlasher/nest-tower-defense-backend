import { SaveChallengeRequest } from '@app/api/game-api';
import { EntityFactory } from '@app/common/modules';
import { Injectable } from '@nestjs/common';

import { PlayerChallengeEntityRepository } from '../../infrastructure/game';
import { PlayerChallenge } from './entities/PlayerChallenge';

@Injectable()
export class PlayerChallengeFactory implements EntityFactory<PlayerChallenge> {
  constructor(
    private readonly playerChallengeEntityRepository: PlayerChallengeEntityRepository,
  ) {}

  async create(
    saveChallengeRequest: SaveChallengeRequest,
  ): Promise<PlayerChallenge> {
    const createdDate = Date.now().toString();

    const entity = new PlayerChallenge({
      ...saveChallengeRequest,
      createdDate,
      isPendingCompletion: true,
    });

    await this.playerChallengeEntityRepository.create(entity);

    return entity;
  }
}
