import { queues } from '@app/api/notification-api';
import {
  EntityDatabaseModule,
  HealthModule,
  RmqModule,
} from '@app/common/modules';
import { PlayerCacheModule } from '@app/common/modules/cache/player-cache.module';
import { PlayerCacheService } from '@app/common/modules/cache/player-cache.service';
import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule, SchemaFactory } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';

import {
  ChallengeEventHandlers,
  ChallengesService,
  GameRoundCommandHandlers,
  GameRoundEventHandlers,
  GameRoundsQueryHandlers,
  ItemCodeCommandHandlers,
  ItemCodeEventsHandlers,
  ItemCodesQueryHandlers,
  LeaderboardAggregationService,
  DailyChallengeService,
  MapMessageCommandHandlers,
  MapMessagesQueryHandlers,
  PlayerChallengeCommandHandlers,
  PlayerChallengeQueryHandlers,
  RoundsReportService,
} from './application/game';
import { GameRoundsController } from './application/game/game-rounds.controller';
import { ItemCodesController } from './application/game/item-codes.controller';
import { PlayerChallengesController } from './application/game/player-challenges.controller';
import { MessageController, MessageService } from './application/message';
import {
  InboxController,
  InboxMessageQueryHandlers,
  PlayerCommandHandlers,
  PlayerController,
  PlayerEventHandlers,
  PlayerQueryHandlers,
  PlayerService,
} from './application/player';
import { ChallengeLeaderboardFactory } from './domain/game/challenge-leaderboard.factory';
import { ChallengeRecordFactory } from './domain/game/challenge-record.factory';
import { GameRoundFactory } from './domain/game/game-round.factory';
import { ItemCodeFactory } from './domain/game/item-code.factory';
import { PlayerChallengeFactory } from './domain/game/player-challenge.factory';
import { InboxMessageFactory, PurchaseFactory } from './domain/player';
import { PlayerFactory } from './domain/player/player.factory';
import {
  ChallengeLeaderboardSchema,
  ChallengeRecordEntityRepository,
  ChallengeRecordSchema,
  ChallengeRecordSchemaFactory,
  ChallenngeRecordsDtoRepository,
  GameRoundEntityRepository,
  GameRoundSchema,
  GameRoundSchemaFactory,
  GameRoundsDtoRepository,
  ItemCodeEntityRepository,
  ItemCodeSchema,
  ItemCodeSchemaFactory,
  ItemCodesDtoRepository,
  MapMessageEntityRepository,
  MapMessageSchema,
  MapMessageSchemaFactory,
  MapMessagesDtoRepository,
  PlayerChallengeEntityRepository,
  PlayerChallengeSchema,
  PlayerChallengeSchemaFactory,
  PlayerChallengesDtoRepository,
} from './infrastructure/game';
import { ChallengeLeaderboardSchemaFactory } from './infrastructure/game/factories/challenge-leaderboard-schema.factory';
import { ChallenngeLeaderboardsDtoRepository } from './infrastructure/game/repositories/challenge-leaderboard-dto.repository';
import { ChallengeLeaderboardEntityRepository } from './infrastructure/game/repositories/challenge-leaderboard-entity.repository';
import {
  InboxMessageSchema,
  MessageDtoRepository,
  MessageSchemaFactory,
  PlayerDtoRepository,
  PlayerSchema,
  PlayerSchemaFactory,
  PurchaseDtoRepository,
  PurchaseSchema,
} from './infrastructure/player';
import InboxMessageEntityRepository from './infrastructure/player/repositories/message-entity.repository';
import { PlayerEntityRepository } from './infrastructure/player/repositories/player-entity.repository';
import { MapMessagesController } from './application/game/map-messages.controller';
import { MapMessageFactory } from './domain/game/map-message.factory';
import { MapMessagesService } from './application/game/services/map-messages.service';
import { DailyChallengeCacheService } from './application/game/services/daily-challenges/challenge.cache.service';

@Module({
  imports: [
    PlayerCacheModule,
    CqrsModule,
    HealthModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: './apps/api-gateway/.env',
    }),
    ScheduleModule.forRoot(),
    EntityDatabaseModule,
    MongooseModule.forFeature([
      {
        name: PlayerChallengeSchema.name,
        schema: SchemaFactory.createForClass(PlayerChallengeSchema),
      },
      {
        name: PlayerSchema.name,
        schema: SchemaFactory.createForClass(PlayerSchema),
      },
      {
        name: InboxMessageSchema.name,
        schema: SchemaFactory.createForClass(InboxMessageSchema),
      },
      {
        name: PurchaseSchema.name,
        schema: SchemaFactory.createForClass(PurchaseSchema),
      },
      {
        name: GameRoundSchema.name,
        schema: SchemaFactory.createForClass(GameRoundSchema),
      },
      {
        name: ItemCodeSchema.name,
        schema: SchemaFactory.createForClass(ItemCodeSchema),
      },
      {
        name: ChallengeRecordSchema.name,
        schema: SchemaFactory.createForClass(ChallengeRecordSchema),
      },
      {
        name: ChallengeLeaderboardSchema.name,
        schema: SchemaFactory.createForClass(ChallengeLeaderboardSchema),
      },
      {
        name: MapMessageSchema.name,
        schema: SchemaFactory.createForClass(MapMessageSchema),
      },
    ]),
    RmqModule.register([{ name: queues.notifications }]),
    CacheModule.register(),
  ],
  controllers: [
    PlayerController,
    InboxController,
    GameRoundsController,
    PlayerChallengesController,
    ItemCodesController,
    MessageController,
    MapMessagesController,
  ],
  providers: [
    // Notification
    MessageService,

    // Player
    PlayerEntityRepository,
    PlayerDtoRepository,
    PlayerSchemaFactory,
    PlayerFactory,
    PlayerService,
    PlayerCacheService,

    //InboxMessage
    InboxMessageEntityRepository,
    MessageDtoRepository,
    MessageSchemaFactory,
    InboxMessageFactory,

    // Purchases
    PurchaseDtoRepository,
    PurchaseSchema,
    PurchaseFactory,

    // Game Rounds
    GameRoundEntityRepository,
    GameRoundsDtoRepository,
    GameRoundSchemaFactory,
    GameRoundFactory,

    // MapMessages
    MapMessageSchema,
    MapMessageFactory,
    MapMessageSchemaFactory,
    MapMessagesDtoRepository,
    MapMessageEntityRepository,

    // Player Challenges
    PlayerChallengeEntityRepository,
    PlayerChallengesDtoRepository,
    PlayerChallengeSchemaFactory,
    PlayerChallengeFactory,
    DailyChallengeCacheService,

    ChallengeRecordEntityRepository,
    ChallenngeRecordsDtoRepository,
    ChallengeRecordSchemaFactory,
    ChallengeRecordFactory,
    ChallengesService,

    ChallengeLeaderboardEntityRepository,
    ChallenngeLeaderboardsDtoRepository,
    ChallengeLeaderboardSchemaFactory,
    ChallengeLeaderboardFactory,

    //Item Codes
    ItemCodeEntityRepository,
    ItemCodesDtoRepository,
    ItemCodeSchemaFactory,
    ItemCodeFactory,

    RoundsReportService,
    LeaderboardAggregationService,
    DailyChallengeService,
    MapMessagesService,

    // Handlers
    ...GameRoundCommandHandlers,
    ...PlayerChallengeCommandHandlers,
    ...ItemCodeCommandHandlers,
    ...MapMessageCommandHandlers,

    ...GameRoundEventHandlers,
    ...ItemCodeEventsHandlers,
    ...ChallengeEventHandlers,

    ...GameRoundsQueryHandlers,
    ...PlayerChallengeQueryHandlers,
    ...ItemCodesQueryHandlers,
    ...MapMessagesQueryHandlers,

    // Handlers
    ...PlayerCommandHandlers,
    ...PlayerEventHandlers,
    ...PlayerQueryHandlers,
    ...InboxMessageQueryHandlers,
  ],
})
export class ApiGatewayModule {}
