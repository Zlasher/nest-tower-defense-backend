import { Observable } from 'rxjs';

import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from '@nestjs/common';

import { Reflector } from '@nestjs/core';

interface HeadersProps {
  authorization: string;
}

@Injectable()
export class AuthGuard implements CanActivate {
  protected readonly logger = new Logger(AuthGuard.name);
  protected readonly authToken: string;

  constructor(private readonly reflector: Reflector) {
    this.authToken = process.env.AUTH_TOKEN;
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const isPublic = this.reflector.get<boolean>(
      'isPublic',
      context.getHandler(),
    );

    if (isPublic) {
      return true;
    }

    const { headers } = context.switchToHttp().getRequest();

    return this.validateAuthorization(headers);
  }

  validateAuthorization(headers: HeadersProps): boolean {
    if (!headers) {
      return false;
    }

    return headers['authorization'] === this.authToken;
  }
}
