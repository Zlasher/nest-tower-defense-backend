import { FilterQuery, Model, SaveOptions, Types } from 'mongoose';

import { IdentifiableEntitySchema } from './identifiable-entity.schema';

export abstract class BaseDtoRepository<
  TSchema extends IdentifiableEntitySchema,
> {
  constructor(protected readonly entityModel: Model<TSchema>) {}

  async create(
    document: Omit<TSchema, '_id'>,
    options?: SaveOptions,
  ): Promise<TSchema> {
    const createdDocument = new this.entityModel({
      ...document,
      _id: new Types.ObjectId(),
    });
    return (await createdDocument.save(options)).toJSON() as unknown as TSchema;
  }

  async find(query: FilterQuery<TSchema>): Promise<TSchema[]> {
    return this.entityModel.find(query, {}, { lean: true });
  }

  async findOne(query: FilterQuery<TSchema>): Promise<TSchema> {
    return this.entityModel.findOne(query, {}, { lean: true });
  }
}
