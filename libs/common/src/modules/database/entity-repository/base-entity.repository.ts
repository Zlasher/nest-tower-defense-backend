import { AggregateRoot } from '@nestjs/cqrs';
import { FilterQuery, Types } from 'mongoose';
import { EntityRepository } from './entity.repository';

import { IdentifiableEntitySchema } from './identifiable-entity.schema';

export abstract class BaseEntityRepository<
  TSchema extends IdentifiableEntitySchema,
  TEntity extends AggregateRoot,
> extends EntityRepository<TSchema, TEntity> {
  async findOneById(id: string): Promise<TEntity> {
    return this.findOne({
      _id: new Types.ObjectId(id),
    } as FilterQuery<TSchema>);
  }

  async findBy(query: FilterQuery<TSchema>): Promise<TEntity[]> {
    return this.find(query);
  }

  async findOneBy(query: FilterQuery<TSchema>): Promise<TEntity> {
    return this.findOne(query);
  }

  async remove(query: FilterQuery<TSchema>): Promise<void> {
    return this.removeAll(query);
  }

  async findOneAndReplaceBy(
    query: FilterQuery<TSchema>,
    entity: TEntity,
  ): Promise<void> {
    await this.findOneAndReplace(query, entity);
  }

  async findAll(): Promise<TEntity[]> {
    return this.find({});
  }
}
