import { SchemaTypes, Types } from 'mongoose';

import { Prop } from '@nestjs/mongoose';

export abstract class IdentifiableEntitySchema {
  @Prop({ type: SchemaTypes.ObjectId })
  _id: Types.ObjectId;
}
