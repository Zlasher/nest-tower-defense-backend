import { IClientConfig } from '@app/common/clients';
import { DynamicModule, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ClientsModule,
  MicroserviceOptions,
  Transport,
} from '@nestjs/microservices';

@Module({
  providers: [],
  exports: [],
})
export class KafkaModule {
  static registerClientsFactory(clients: IClientConfig[]): DynamicModule {
    return {
      module: KafkaModule,
      imports: [
        ClientsModule.registerAsync(
          clients.map(({ name, clientId, consumer }) => {
            return {
              name: name,
              // eslint-disable-next-line @typescript-eslint/no-unused-vars
              useFactory: (_configService: ConfigService) => ({
                transport: Transport.KAFKA,
                options: {
                  client: {
                    clientId: clientId,
                    brokers: ['kafka:9093'],
                  },
                  consumer: {
                    groupId: consumer,
                  },
                },
              }),
              inject: [ConfigService],
            };
          }),
        ),
      ],
      exports: [ClientsModule],
    };
  }

  static registerService({ consumer }: IClientConfig): MicroserviceOptions {
    return {
      transport: Transport.KAFKA,
      options: {
        client: {
          brokers: ['kafka:9093'],
        },
        consumer: {
          groupId: consumer,
        },
      },
    };
  }
}
