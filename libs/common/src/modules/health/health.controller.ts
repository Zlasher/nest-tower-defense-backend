import { SetMetadata } from '@nestjs/common';

import { Controller, Get } from '@nestjs/common';

import { HealthService } from './health.service';

export const Public = () => SetMetadata('isPublic', true);

@Controller('health')
export class HealthController {
  constructor(private readonly healthService: HealthService) {}

  @Public()
  @Get()
  async getHealth() {
    return this.healthService.getHealth();
  }
}
