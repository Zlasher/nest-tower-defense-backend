import { Injectable } from '@nestjs/common';
import { hostname, uptime, totalmem, freemem, loadavg } from 'os';
import { getMemoryReport, getUptimeReport } from './utils';
import { appVersion } from '@app/common';

@Injectable()
export class HealthService {
  getHealth() {
    return {
      status: 'Up',
      version: appVersion,
      host: hostname(),
      loadavg: loadavg(),
      uptime: getUptimeReport(uptime()),
      memory: {
        total: getMemoryReport(totalmem()),
        free: getMemoryReport(freemem()),
      },
    };
  }
}
