export const getUptimeReport = (uptimeSeconds: number): string => {
  let ut_sec = uptimeSeconds;
  let ut_min = ut_sec / 60;
  let ut_hour = ut_min / 60;

  ut_sec = Math.floor(ut_sec);
  ut_min = Math.floor(ut_min);
  ut_hour = Math.floor(ut_hour);

  ut_hour = ut_hour % 60;
  ut_min = ut_min % 60;
  ut_sec = ut_sec % 60;

  return `${ut_hour} Hour(s) ${ut_min} minute(s) and ${ut_sec} second(s)`;
};

export const getMemoryReport = (memory: number): string => {
  let total_memory = memory;
  let total_mem_in_kb = total_memory / 1024;
  let total_mem_in_mb = total_mem_in_kb / 1024;
  let total_mem_in_gb = total_mem_in_mb / 1024;

  total_mem_in_kb = Math.floor(total_mem_in_kb);
  total_mem_in_mb = Math.floor(total_mem_in_mb);
  total_mem_in_gb = Math.floor(total_mem_in_gb);

  total_mem_in_mb = total_mem_in_mb % 1024;
  total_mem_in_kb = total_mem_in_kb % 1024;
  total_memory = total_memory % 1024;

  return `${total_mem_in_gb} GB ${total_mem_in_mb} MB`;
};
