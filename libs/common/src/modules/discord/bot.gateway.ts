import { InjectDiscordClient, Once } from '@discord-nestjs/core';
import { Injectable, Logger } from '@nestjs/common';
import { Client } from 'discord.js';

@Injectable()
export class BotGateway {
  private readonly logger = new Logger(BotGateway.name);

  constructor(
    @InjectDiscordClient()
    private readonly client: Client,
  ) {}

  @Once('ready')
  onReady() {
    this.logger.log(`Bot ${this.client.user.tag} was started!`);
  }

  // // @On('messageCreate')
  // @UseGuards(MessageFromUserGuard)
  // @UsePipes(MessageToUpperPipe)
  // async onMessage(message: Message): Promise<void> {
  //   // this.logger.log(`Incoming message: ${message.content}`);

  //   // await message.reply('Message processed successfully');
  // }
}
