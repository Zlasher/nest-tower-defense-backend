import {
  DiscordModuleOption,
  DiscordOptionsFactory,
  TransformPipe,
  ValidationPipe,
} from 'discord-nestjs';

import { Injectable } from '@nestjs/common';

@Injectable()
export class DiscordConfigService implements DiscordOptionsFactory {
  createDiscordOptions(): DiscordModuleOption {
    const config: DiscordModuleOption = {
      token: process.env.DISCORD_BOT_TOKEN,
      commandPrefix: '!',
      allowGuilds: [],
      denyGuilds: [],
      allowCommands: [],
      webhook: {
        webhookId: process.env.DISCORD_WEBHOOK_INFO_CHANNEL_ID,
        webhookToken: process.env.DISCORD_WEBHOOK_INFO_TOKEN,
      },
      usePipes: [TransformPipe, ValidationPipe],
    };

    console.log(config);
    return config;
  }
}
