/* eslint-disable @typescript-eslint/no-unused-vars */
import { DynamicModule, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { GrpcService } from './grpc.service';

interface GrpcModuleOptions {
  name: string;
}

@Module({
  providers: [GrpcService],
  exports: [GrpcService],
})
export class GrpcModule {
  static register({ name }: GrpcModuleOptions): DynamicModule {
    return {
      module: GrpcModule,
      imports: [
        ClientsModule.registerAsync([
          {
            name,
            useFactory: (_configService: ConfigService) => ({
              transport: Transport.GRPC,
              options: {
                package: name,
                protoPath: join(__dirname, `${name}.proto`),
              },
            }),
            inject: [ConfigService],
          },
        ]),
      ],
      exports: [ClientsModule],
    };
  }
}
