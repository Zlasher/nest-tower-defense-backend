import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { GrpcOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

@Injectable()
export class GrpcService {
  constructor(private readonly configService: ConfigService) {}

  getOptions(name: string): GrpcOptions {
    return {
      transport: Transport.GRPC,
      options: {
        package: name,
        protoPath: join(__dirname, `${name}.proto`),
      },
    };
  }
}
