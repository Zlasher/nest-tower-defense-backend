import { Cache, CachingConfig } from 'cache-manager';

import { ByPlayerIdRequest, PlayerDTO } from '@app/api/player-api';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Inject, Injectable } from '@nestjs/common';

const cacheConfig: CachingConfig = {
  ttl: 60 * 60 * 6, // 6 Hours Cache,
};

@Injectable()
export class PlayerCacheService {
  constructor(
    @Inject(CACHE_MANAGER)
    private cacheManager: Cache,
  ) {}

  async getById({ playerId }: ByPlayerIdRequest): Promise<PlayerDTO | null> {
    return this.cacheManager
      .get<PlayerDTO | null>(`player-${playerId}`)
      .catch(() => null);
  }

  async setById(player: PlayerDTO): Promise<PlayerDTO | null> {
    return this.cacheManager
      .set<PlayerDTO>(`player-${player.playerId}`, player, cacheConfig)
      .catch(() => null);
  }
}
