import { PlayerDTO } from '@app/api/player-api';
import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';

import { PlayerCacheModule } from '../player-cache.module';
import { PlayerCacheService } from '../player-cache.service';

const player: PlayerDTO = {
  playerId: '1',
  name: 'John Doe',
  device: 'Samsung X',
  isPremium: false,
  discordId: 'AfA2345312',
  gameVersion: '2.0.62',
  createdDate: 'Tue, 31 May 2022 18:46:23 GMT',
  lastLogin: 'Tue, 31 May 2022 18:46:23 GMT',
};

const createService = (): Promise<PlayerCacheService> =>
  Test.createTestingModule({
    imports: [PlayerCacheModule, CacheModule.register()],
    providers: [PlayerCacheService],
  })
    .compile()
    .then((module: TestingModule) => module.get(PlayerCacheService));

describe('PlayerCacheService', () => {
  let service: PlayerCacheService;

  beforeEach(async () => {
    service = await createService();
  });

  it('service should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getById', () => {
    it('should resolve', async () => {
      jest.spyOn(service, 'getById').mockResolvedValue(player);

      expect(await service.getById({ playerId: player.playerId })).toEqual(
        player,
      );
    });
  });

  describe('setById', () => {
    it('should resolve', async () => {
      jest.spyOn(service, 'setById').mockResolvedValue(player);

      expect(await service.setById(player)).toEqual(player);
    });
  });
});
