import { Module } from '@nestjs/common';

import { PlayerCacheService } from './player-cache.service';
import { CacheModule } from '@nestjs/cache-manager';

@Module({
  imports: [CacheModule.register()],
  controllers: [],
  providers: [PlayerCacheService],
})
export class PlayerCacheModule {}
