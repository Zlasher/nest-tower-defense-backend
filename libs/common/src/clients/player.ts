import { IClientConfig } from './types';

export const playerClientConfig: IClientConfig = {
  name: 'PLAYER_SERVICE',
  clientId: 'player',
  consumer: 'player-consumer',
};
