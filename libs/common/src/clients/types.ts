export interface IClientConfig {
  name: string;
  clientId: string;
  consumer: string;
}
