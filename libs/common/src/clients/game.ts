import { IClientConfig } from './types';

export const gameClientConfig: IClientConfig = {
  name: 'GAME_SERVICE',
  clientId: 'game',
  consumer: 'game-consumer',
};
