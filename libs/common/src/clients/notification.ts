import { IClientConfig } from './types';

export const notificationClientConfig: IClientConfig = {
  name: 'NOTIFICATION_SERVICE',
  clientId: 'notification',
  consumer: 'notification-consumer',
};
