import { IsArray, IsNotEmpty, IsString } from 'class-validator';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

import { IChallenge } from '../../interfaces';

export class SaveChallengeRequest
  implements Omit<IChallenge, '_id' | 'isPendingCompletion'>
{
  @IsString()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: false })
  challengeId?: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'LEVEL_GRASSLANDS_1', required: true })
  mapId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'My Ultimate Challenge', required: true })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'To test your dps skillz', required: true })
  description: string;

  @IsArray()
  @ApiProperty({
    isArray: true,
    example: ['SOME_MODIFIER', 'SOME_OTHER_MODFIFIER'],
    required: false,
    type: String,
  })
  modifiers: string[];

  @IsArray()
  @ApiProperty({
    isArray: true,
    example: ['TOWER_1', 'TOWER_2'],
    required: false,
    type: String,
  })
  excludedTowers: string[];

  @IsArray()
  @ApiProperty({
    isArray: true,
    example: ['Tower_3', 'Tower_4'],
    required: false,
    type: String,
  })
  allowedTowers: string[];

  constructor(
    playerId: string,
    mapId: string,
    name: string,
    description: string,
    modifiers: string[],
    excludedTowers: string[] = [],
    allowedTowers: string[] = [],
  ) {
    this.playerId = playerId;
    this.mapId = mapId;
    this.name = name;
    this.description = description;
    this.modifiers = modifiers;
    this.excludedTowers = excludedTowers;
    this.allowedTowers = allowedTowers;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      playerId: this.playerId,
      mapId: this.mapId,
      name: this.name,
      description: this.description,
      modifiers: this.modifiers,
      excludedTowers: this.excludedTowers,
      allowedTowers: this.allowedTowers,
    });
  }
}
