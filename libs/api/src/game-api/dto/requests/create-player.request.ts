import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

import { IGame } from '../../interfaces';

export class CreateGameRoundRequest implements Omit<IGame, 'date'> {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'LEVEL_GRASSLANDS_1', required: true })
  mapId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '5/5', required: true })
  wavesProgress: string;

  @IsNumber()
  @ApiProperty({ example: 12, required: true })
  lives: number;

  @IsNumber()
  @ApiProperty({ example: 502, required: true })
  seconds: number;

  @IsNumber()
  @ApiProperty({ example: 2, required: true })
  tier: number;

  @ApiProperty({ example: 2, required: false, nullable: true })
  challengeId: string;

  constructor(
    playerId: string,
    mapId: string,
    waveProgress: string,
    lives: number,
    seconds: number,
    tier: number,
  ) {
    this.playerId = playerId;
    this.mapId = mapId;
    this.wavesProgress = waveProgress;
    this.lives = lives;
    this.seconds = seconds;
    this.tier = tier;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      playerId: this.playerId,
      mapId: this.mapId,
      wavesProgress: this.wavesProgress,
      lives: this.lives,
      seconds: this.seconds,
      tier: this.tier,
    });
  }

  public setChallengeId(challengeId: string): void {
    this.challengeId = challengeId;
  }
}
