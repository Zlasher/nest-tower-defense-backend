import { IsArray, IsNotEmpty, IsString } from 'class-validator';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

import {
  IChallengeLeaderboard,
  ILeaderboardEntry,
} from '../../interfaces/IChallengeLeaderboard';

export class UpdateChallengeLeaderboardRequest
  implements Omit<IChallengeLeaderboard, '_id'>
{
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  challengeId: string;

  @IsArray()
  @ApiProperty({
    isArray: true,
    example: [],
    required: false,
  })
  entries: ILeaderboardEntry[];

  constructor(challengeId: string, entries: ILeaderboardEntry[]) {
    this.challengeId = challengeId;
    this.entries = entries;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      challengeId: this.challengeId,
      entries: this.entries,
    });
  }
}
