import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

import { IChallengeRecord } from '../../interfaces/IChallengeRecord';

export class CreateChallengeRecordRequest
  implements Omit<IChallengeRecord, '_id' | 'createdDate'>
{
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  challengeId: string;

  @IsNumber()
  @ApiProperty({ example: 192, required: true })
  seconds: number;

  constructor(challengeId: string, playerId: string, seconds: number) {
    this.challengeId = challengeId;
    this.playerId = playerId;
    this.seconds = seconds;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      challengeId: this.challengeId,
      playerId: this.playerId,
      seconds: this.seconds,
    });
  }
}
