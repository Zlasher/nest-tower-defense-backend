import { IsNotEmpty, IsString } from 'class-validator';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

import { IItemCode } from '../../interfaces';

export class ClaimItemCodeRequest implements Pick<IItemCode, 'code'> {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'code-1', required: true })
  code: string;

  constructor(playerId: string, code: string) {
    this.playerId = playerId;
    this.code = code;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      playerId: this.playerId,
      code: this.code,
    });
  }
}

export class ClaimItemCodeResponse
  implements Partial<Omit<IItemCode, 'isUsed'>>
{
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  _id?: any;

  @ApiProperty({ example: 'code-1', required: true })
  code?: string;

  @ApiProperty({ example: 'chest-ITEM_NAME', required: true })
  itemName?: string;

  @ApiProperty({ example: false, required: true })
  isError?: boolean;
}
