import { IsNotEmpty, IsString } from 'class-validator';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { ItemCodeDTO } from '../item-code.dto';

export class CreateItemCodesRequest {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'code-1,code-2', required: true })
  codeInput: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'chest-SOME_ITEM_X', required: true })
  itemName: string;

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      codeInput: this.codeInput,
      itemName: this.itemName,
    });
  }
}

export class CreateItemCodesReponse extends CreateItemCodesRequest {
  @ApiProperty({
    example: ['code-1', 'code-2'],
    isArray: true,
    type: ItemCodeDTO,
  })
  codes: ItemCodeDTO[];
}
