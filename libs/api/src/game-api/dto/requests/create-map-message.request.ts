import { IsNotEmpty, IsString } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

import { IMapMessage } from '../../interfaces';

export class CreateMapMessageRequest
  implements Omit<IMapMessage, '_id' | 'createdAt'>
{
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'LEVEL_GRASSLANDS_1', required: true })
  mapId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '5/5', required: true })
  message: string;

  @ApiProperty({ example: '5/5', required: true })
  position: string;

  @ApiProperty({ example: '5/5', required: true })
  createdBy: string;

  constructor(
    mapId: string,
    message: string,
    position: string,
    createdBy: string,
  ) {
    this.mapId = mapId;
    this.message = message;
    this.position = position;
    this.createdBy = createdBy;
  }
}
