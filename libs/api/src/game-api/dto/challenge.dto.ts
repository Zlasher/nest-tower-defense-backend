import { ApiProperty } from '@nestjs/swagger';
import { IChallenge } from '../interfaces';

export class PlayerChallengeDto implements Omit<IChallenge, '_id'> {
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly challengeId: any;

  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly playerId: string;

  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly mapId: string;

  @ApiProperty({ example: true, required: true })
  readonly isPendingCompletion: boolean;

  @ApiProperty({ example: 'Some name', required: true })
  readonly name: string;

  @ApiProperty({ example: 'Some description here', required: true })
  readonly description: string;

  @ApiProperty({ example: '123456789', required: false })
  readonly createdDate: string;

  @ApiProperty({
    isArray: true,
    example: ['SOME_MODIFIER', 'SOME_OTHER_MODFIFIER'],
    required: false,
    type: String,
  })
  modifiers: string[];

  @ApiProperty({
    isArray: true,
    example: ['Tower_1', 'Tower_2'],
    required: false,
    type: String,
  })
  excludedTowers: string[];

  @ApiProperty({
    isArray: true,
    example: ['Tower_1', 'Tower_2'],
    required: false,
    type: String,
  })
  allowedTowers: string[];

  constructor(challenge: IChallenge) {
    this.challengeId = challenge._id;
    this.playerId = challenge.playerId;
    this.isPendingCompletion = challenge.isPendingCompletion;
    this.mapId = challenge.mapId;
    this.name = challenge.name;
    this.description = challenge.description;
    this.modifiers = challenge.modifiers;
    this.excludedTowers = challenge.excludedTowers;
    this.allowedTowers = challenge.allowedTowers;
    this.createdDate = new Date(Number(challenge.createdDate)).toUTCString();
  }

  toString() {
    return JSON.stringify({
      ...new PlayerChallengeDto(this),
    });
  }
}
