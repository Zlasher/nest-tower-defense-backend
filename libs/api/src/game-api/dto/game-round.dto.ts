import { ApiProperty } from '@nestjs/swagger';
import { IGame } from '../interfaces';

export class GameRoundDTO implements Omit<IGame, '_id'> {
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly roundId: any;

  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly playerId: string;

  @ApiProperty({ example: 'LEVEL_GRASSLANDS_12', required: true })
  readonly mapId: string;

  @ApiProperty({ example: '5/7', required: true })
  readonly wavesProgress: string;

  @ApiProperty({ example: 12, required: true })
  readonly lives: number;

  @ApiProperty({ example: 512, required: true })
  readonly seconds: number;

  @ApiProperty({ example: 2, required: true })
  readonly tier: number;

  @ApiProperty({ example: 'Tue, 31 May 2022 18:46:23 GMT', required: true })
  readonly date: string;

  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly challengeId?: string;

  constructor(game: IGame) {
    this.roundId = game._id;
    this.playerId = game.playerId;
    this.mapId = game.mapId;
    this.wavesProgress = game.wavesProgress;
    this.lives = game.lives;
    this.seconds = game.seconds;
    this.tier = game.tier;
    this.challengeId = game.challengeId;

    this.date = new Date(Number(game.date)).toUTCString();
  }

  toString() {
    return JSON.stringify({
      ...new GameRoundDTO(this),
    });
  }
}
