import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

import { IItemCode } from '../interfaces';

export class ItemCodeDTO implements Omit<IItemCode, '_id'> {
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: false })
  readonly itemId: any;

  @ApiProperty({ example: 'item-code-1', required: false })
  readonly code: string;

  @ApiProperty({ example: 'chest-SOME_COOL_CHEST', required: false })
  readonly itemName: string;

  @ApiProperty({ example: true, required: false })
  readonly isUsed: boolean;

  constructor(itemCode: IItemCode) {
    this.itemId = itemCode._id;
    this.code = itemCode.code;
    this.itemName = itemCode.itemName;
    this.isUsed = Boolean(itemCode.isUsed);
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      ...new ItemCodeDTO(this),
    });
  }
}
