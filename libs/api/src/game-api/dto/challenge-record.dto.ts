import { ApiProperty } from '@nestjs/swagger';
import { IChallengeRecord } from '../interfaces/IChallengeRecord';

export class ChallengeRecordDto implements Omit<IChallengeRecord, '_id'> {
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly recordId: any;

  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly challengeId: any;

  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly playerId: string;

  @ApiProperty({ example: 192, required: true })
  readonly seconds: number;

  @ApiProperty({ example: '123456789', required: false })
  readonly createdDate: string;

  constructor(challenge: IChallengeRecord) {
    this.recordId = challenge._id;
    this.playerId = challenge.playerId;
    this.challengeId = challenge.challengeId;
    this.seconds = challenge.seconds;

    this.createdDate = new Date(Number(challenge.createdDate)).toUTCString();
  }

  toString() {
    return JSON.stringify({
      ...new ChallengeRecordDto(this),
    });
  }
}
