import { IsArray } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

import { IChallengeLeaderboard } from '../interfaces/IChallengeLeaderboard';
import { ChallengeLeaderboarEntrydDto } from './challenge-leaderboard-entry.dto';

export class ChallengeLeaderboardDto
  implements Omit<IChallengeLeaderboard, '_id'>
{
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly leaderboardId: any;

  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly challengeId: any;

  @IsArray()
  @ApiProperty({
    isArray: true,
    type: ChallengeLeaderboarEntrydDto,
    example: [
      {
        playerId: 'PlayerName',
        seconds: 98,
      },
    ],
    required: true,
  })
  readonly entries: ChallengeLeaderboarEntrydDto[];

  constructor(challenge: IChallengeLeaderboard) {
    this.leaderboardId = challenge._id;
    this.challengeId = challenge.challengeId;

    this.entries = challenge.entries.map(
      (entry) => new ChallengeLeaderboarEntrydDto(entry),
    );
  }

  toString() {
    return JSON.stringify({
      ...new ChallengeLeaderboardDto(this),
    });
  }
}
