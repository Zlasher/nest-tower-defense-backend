import { PlayerChallengeDto } from '@app/api/game-api';
import { ApiProperty } from '@nestjs/swagger';

export class ChallengesQuery {
  @ApiProperty({ example: '98JADAWFAW76ADA', required: false, nullable: true })
  public playerId?: string;

  @ApiProperty({ example: '98JADAWFAW76ADA', required: false, nullable: true })
  public mapId?: string;

  @ApiProperty({ example: '98JADAWFAW76ADA', required: false, nullable: true })
  public name?: string;

  constructor(params: Partial<ChallengesQuery> = {}) {
    this.playerId = params.playerId;
    this.mapId = params.mapId;
    this.name = params.name;
  }

  copy(fromQuery: Partial<ChallengesQuery>): ChallengesQuery {
    this.playerId = fromQuery.playerId;
    this.mapId = fromQuery.mapId;
    this.name = fromQuery.name;

    return this;
  }
}

export class ChallengesResponse {
  @ApiProperty({
    required: true,
    type: PlayerChallengeDto,
    isArray: true,
  })
  challenges: PlayerChallengeDto[];
}
