import { IsNotEmpty, IsString } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class RoundsByPlayerQuery {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '98JADAWFAW76ADA', required: true })
  playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'LEVEL_GRASSLANDS_1', required: true })
  mapId: string;
}
