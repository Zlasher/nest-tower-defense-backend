import { IsNumber } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { GameRoundDTO } from '../game-round.dto';

export class GameRoundsQuery {
  @ApiProperty({ example: '1704661070082', required: false, nullable: true })
  date?: string;

  @ApiProperty({ example: '98JADAWFAW76ADA', required: false, nullable: true })
  playerId?: string;

  @ApiProperty({ example: 'win', required: false, nullable: true })
  state?: 'win' | 'lose';

  @ApiProperty({ example: 2, required: false, nullable: true })
  tier: number;

  @ApiProperty({ example: 2, required: false, nullable: true })
  daysBack: number;

  copy(fromQuery: Partial<GameRoundsQuery>): GameRoundsQuery {
    this.date = fromQuery.date;
    this.playerId = fromQuery.playerId;
    this.state = fromQuery.state;
    this.tier = fromQuery.tier;
    this.daysBack = fromQuery.daysBack;

    return this;
  }
}

export class GameRoundsResponse {
  @IsNumber()
  @ApiProperty({ required: true, example: 32 })
  count: number;

  @ApiProperty({ isArray: true, required: true, type: GameRoundDTO })
  rounds: GameRoundDTO[];
}
