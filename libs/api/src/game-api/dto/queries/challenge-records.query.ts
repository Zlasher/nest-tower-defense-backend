import { ApiProperty } from '@nestjs/swagger';

import { ChallengeRecordDto } from '../challenge-record.dto';

export class ChallengeRecordsQuery {
  @ApiProperty({ example: '98JADAWFAW76ADA', required: false, nullable: true })
  public challengeId?: string;

  @ApiProperty({ example: '98JADAWFAW76ADA', required: false, nullable: true })
  public sinceDate?: string;

  constructor({ challengeId, sinceDate }: Partial<ChallengeRecordsQuery> = {}) {
    this.challengeId = challengeId;
    this.sinceDate = sinceDate;
  }
}

export class ChallengeRecordsResponse {
  @ApiProperty({
    required: true,
    type: ChallengeRecordDto,
    isArray: true,
    example: [],
  })
  records: ChallengeRecordDto[];
}
