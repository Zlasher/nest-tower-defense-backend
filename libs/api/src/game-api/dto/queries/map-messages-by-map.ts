import { IsNotEmpty, IsString } from 'class-validator';

import { MapMessageDto } from '@app/api/game-api';
import { ApiProperty } from '@nestjs/swagger';

export class GetMessageByMapQuery {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '98JADAWFAW76ADA', required: true })
  mapId: string;

  constructor(mapId: string) {
    this.mapId = mapId;
  }
}

export class GetMessageByMapResponse {
  @ApiProperty({
    type: MapMessageDto,
    example: {
      mapId: 'LEVEL_GRASSLANDS_3',
      message: 'Hello world!',
      position: '0.0,1.1,2.2',
      createdBy: 'Johan',
      createdAt: new Date().toUTCString(),
    },
  })
  messageDto: MapMessageDto | null;
}
