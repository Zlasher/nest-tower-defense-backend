import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

import { GameRoundDTO } from '../game-round.dto';

export class PlayerAndMapQuery {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '98JADAWFAW76ADA', required: true })
  playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'LEVEL_GRASSLANDS_1', required: true })
  mapId: string;
}

export class GameRoundsByPlayerAndMapResponse extends PlayerAndMapQuery {
  @IsNumber()
  @ApiProperty({ required: true, example: 32 })
  count: number;

  @ApiProperty({ isArray: true, required: true, type: GameRoundDTO })
  rounds: GameRoundDTO[];
}
