import { ChallengeLeaderboardDto } from '@app/api/game-api';
import { ApiProperty } from '@nestjs/swagger';

export class ChallengeLeaderboardsQuery {
  @ApiProperty({ example: '98JADAWFAW76ADA', required: false, nullable: true })
  public challengeId?: string;

  constructor({ challengeId }: Partial<ChallengeLeaderboardsQuery> = {}) {
    this.challengeId = challengeId;
  }
}

export class ChallengeLeaderboardsResponse {
  @ApiProperty({
    required: true,
    type: ChallengeLeaderboardDto,
    isArray: true,
    example: [],
  })
  leaderboards: ChallengeLeaderboardDto[];
}
