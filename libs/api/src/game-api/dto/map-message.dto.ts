import { ApiProperty } from '@nestjs/swagger';

import { IMapMessage } from '../interfaces';

export class MapMessageDto implements Omit<IMapMessage, '_id'> {
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: false })
  readonly mapId: string;

  @ApiProperty({ example: 'Hello world!', required: true })
  readonly message: string;

  @ApiProperty({ example: '{ x: 0, y :1, z: 2}', required: true })
  readonly position: string;

  @ApiProperty({ example: 'Hello world!', required: true })
  readonly createdBy: string;

  @ApiProperty({ example: 'Hello world!', required: true })
  readonly createdAt: string;

  constructor(mapMessage: IMapMessage) {
    this.mapId = mapMessage.mapId;
    this.message = mapMessage.message;
    this.position = mapMessage.position;
    this.createdBy = mapMessage.createdBy;

    this.createdAt = new Date(Number(mapMessage.createdAt)).toUTCString();
  }
}
