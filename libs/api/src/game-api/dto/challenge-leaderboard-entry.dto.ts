import { ApiProperty } from '@nestjs/swagger';
import { ILeaderboardEntry } from '../interfaces/IChallengeLeaderboard';

export class ChallengeLeaderboarEntrydDto implements ILeaderboardEntry {
  @ApiProperty({
    description: 'Sent as player name',
    example: 'Player Name',
    required: true,
  })
  playerId: any;

  @ApiProperty({ example: 192, required: true })
  readonly seconds: number;

  constructor(challenge: ILeaderboardEntry) {
    this.playerId = challenge.playerId;
    this.seconds = challenge.seconds;
  }

  toString() {
    return JSON.stringify({
      ...new ChallengeLeaderboarEntrydDto(this),
    });
  }
}
