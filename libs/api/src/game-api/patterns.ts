export const patterns = {
  getRoundsForPlayer: 'get_rounds_for_player',
  getAllCodes: 'get_all_codes',
  createCodes: 'create_codes',
  claimCode: 'claim_item_code',
  createMapMessage: 'create_map_message',
};
