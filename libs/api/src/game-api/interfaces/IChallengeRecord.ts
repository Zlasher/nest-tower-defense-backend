export interface IChallengeRecord {
  _id?: any;
  challengeId: any;
  playerId: string;
  seconds: number;
  createdDate: string;
}
