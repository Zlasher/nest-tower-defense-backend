export interface IMapMessage {
  _id?: any;
  mapId: string;
  message: string;
  position: string;

  createdBy: string;
  createdAt: string;
}
