export interface IGame {
  _id?: any;
  playerId: string;
  mapId: string;
  wavesProgress: string;
  lives: number;
  seconds: number;
  tier: number;
  date: string;
  challengeId?: string;
}
