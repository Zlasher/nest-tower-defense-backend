export interface IItemCode {
  _id?: any;
  code: string;
  itemName: string;
  isUsed: boolean;
}
