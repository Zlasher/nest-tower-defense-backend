export interface IChallengeLeaderboard {
  _id?: any;
  challengeId: any;
  entries: ILeaderboardEntry[];
}

export interface ILeaderboardEntry {
  playerId: string;
  seconds: number;
}
