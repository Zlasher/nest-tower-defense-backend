export interface IChallenge {
  _id?: any;
  playerId: string;
  mapId: string;
  isPendingCompletion: boolean;
  name: string;
  description: string;
  modifiers?: string[];
  excludedTowers?: string[];
  allowedTowers?: string[];
  createdDate?: string;
}
