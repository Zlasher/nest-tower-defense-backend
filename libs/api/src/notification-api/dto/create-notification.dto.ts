import { IsNotEmpty, IsString } from 'class-validator';
import { ChannelType } from '../interfaces/ChannelType';
import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

export class CreateNotificationDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    example: ChannelType.Notification,
    required: true,
    enum: ChannelType,
    default: ChannelType.Notification,
    enumName: 'ChannelType',
  })
  readonly channelType: ChannelType;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Some event that happened', required: true })
  readonly message: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '2.0.62', required: true })
  readonly gameVersion: string;

  constructor(
    playerId: string,
    message: string,
    gameVersion: string,
    channelType: ChannelType = ChannelType.Notification,
  ) {
    this.playerId = playerId;
    this.channelType = channelType;
    this.message = message;
    this.gameVersion = gameVersion;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      playerId: this.playerId,
      channelType: this.channelType,
      message: this.message,
      gameVersion: this.gameVersion,
    });
  }
}
