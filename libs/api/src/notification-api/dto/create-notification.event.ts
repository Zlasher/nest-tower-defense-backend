import { IsNotEmpty, IsString } from 'class-validator';
import { ChannelType } from '../interfaces/ChannelType';
import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

export class CreateNotificationEvent {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    example: ChannelType.Notification,
    required: true,
    enum: ChannelType,
  })
  readonly channelType: ChannelType;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Some message', required: true })
  readonly message: string;

  constructor(channelType: ChannelType, message: string) {
    this.channelType = channelType;
    this.message = message;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      channelType: this.channelType,
      message: this.message,
    });
  }
}
