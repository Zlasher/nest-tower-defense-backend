export interface INotification {
  playerId: string;
  type: string;
  message: string;
  date: string;
  gameVersion?: string;
}
