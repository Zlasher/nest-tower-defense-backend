export enum ChannelType {
  Notification = 0,
  Feedback = 1,
  Arena = 2,
  Purchases = 3,
}
