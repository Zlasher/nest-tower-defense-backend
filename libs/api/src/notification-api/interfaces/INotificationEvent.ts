import { CreateNotificationEvent } from '../dto';

export interface INotificationEvent<T> {
  createNotification(
    request: T,
  ): CreateNotificationEvent | Promise<CreateNotificationEvent>;
}
