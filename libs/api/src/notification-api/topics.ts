export const topics = {
  createNotification: 'create_notification',
};

export const queues = {
  notifications: 'NOTIFICATION',
};
