export interface IMessageInbox {
  _id?: any;
  playerId: string;
  message: string;
  date: string;
  isRead: boolean;
}
