export interface IPurchase {
  playerId: string;
  itemId: string;
  date: string;
}
