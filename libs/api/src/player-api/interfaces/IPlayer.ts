export interface IPlayer {
  _id?: any;
  playerId: string;
  name: string;
  device: string;
  createdDate: string;
  lastLogin: string;
  gameVersion: string;
  isPremium: boolean;
  discordId: string;
}
