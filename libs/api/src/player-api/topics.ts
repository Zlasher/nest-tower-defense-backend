export const topics = {
  // Player
  playerLogin: 'player_login',
  playerSignup: 'player_signup',
  playerPurchase: 'player_purchase',
  playerPromote: 'player_promote',

  // Inbox
  createMessage: 'player_create_message',
  setMessageRead: 'player_set_message_read',
};
