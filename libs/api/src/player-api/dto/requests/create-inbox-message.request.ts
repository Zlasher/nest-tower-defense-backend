import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateInboxMessageRequest {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Hello, example message', required: true })
  message: string;

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      playerId: this.playerId,
      message: this.message,
    });
  }
}
