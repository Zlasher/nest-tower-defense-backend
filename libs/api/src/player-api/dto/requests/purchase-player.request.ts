import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class PurchasePlayerRequest {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'remove_ads_item', required: true })
  itemId: string;

  constructor(playerId: string, itemId: string) {
    this.playerId = playerId;
    this.itemId = itemId;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      playerId: this.playerId,
      itemId: this.itemId,
    });
  }
}
