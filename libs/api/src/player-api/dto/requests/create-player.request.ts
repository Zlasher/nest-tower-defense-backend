import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class CreatePlayerRequest {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  playerId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Batman', required: true })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Samsumg S10 X10', required: true })
  device: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '2.0.62', required: true })
  gameVersion: string;

  constructor(
    playerId: string,
    name: string,
    device: string,
    gameVersion: string,
  ) {
    this.playerId = playerId;
    this.name = name;
    this.device = device;
    this.gameVersion = gameVersion;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      playerId: this.playerId,
      name: this.name,
      device: this.device,
      gameVersion: this.gameVersion,
    });
  }
}
