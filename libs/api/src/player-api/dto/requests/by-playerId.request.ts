import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class ByPlayerIdRequest {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  playerId: string;

  constructor(playerId: string) {
    this.playerId = playerId;
  }

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      playerId: this.playerId,
    });
  }
}
