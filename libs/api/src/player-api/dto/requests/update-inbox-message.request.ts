import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateInboxMessageRequest {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'AD172317H231796AHDWAD7WDAW6DAH', required: true })
  messageId: string;

  @ApiHideProperty()
  toString() {
    return JSON.stringify({
      messageId: this.messageId,
    });
  }
}
