import { ApiProperty } from '@nestjs/swagger';

export class IsPlayerPremiumResponse {
  @ApiProperty({ example: true, required: true })
  isPremium: boolean;
}
