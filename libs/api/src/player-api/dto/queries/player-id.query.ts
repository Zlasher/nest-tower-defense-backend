import { IsNotEmpty, IsString } from 'class-validator';
import { ByPlayerIdRequest } from '../requests/by-playerId.request';
import { ApiProperty } from '@nestjs/swagger';

export abstract class PlayerIdQuery {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  public readonly playerId: string;

  constructor(query: ByPlayerIdRequest) {
    this.playerId = query.playerId;
  }
}
