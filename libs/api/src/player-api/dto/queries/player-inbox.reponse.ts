import { ApiProperty } from '@nestjs/swagger';
import { InboxMessageDTO } from '../inbox-message.dto';

export class PlayerInboxResponse {
  @ApiProperty({
    isArray: true,
    example: [],
    required: true,
    type: InboxMessageDTO,
  })
  messages: InboxMessageDTO[];
}
