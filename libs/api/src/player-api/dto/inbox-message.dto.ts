import { ApiProperty } from '@nestjs/swagger';

import { IMessageInbox } from '../interfaces';

export class InboxMessageDTO implements Omit<IMessageInbox, 'isRead'> {
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly messageId: string;

  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly playerId: string;

  @ApiProperty({ example: 'Hello, nice message you got there', required: true })
  readonly message: string;

  @ApiProperty({ example: '123456799', required: true })
  readonly date: string;

  constructor(message: IMessageInbox) {
    this.messageId = message._id;
    this.playerId = message.playerId;
    this.message = message.message;

    this.date = new Date(Number(message.date)).toUTCString();
  }

  toString() {
    const dto: InboxMessageDTO = {
      playerId: this.playerId,
      messageId: this.messageId,
      message: this.message,
      date: this.date,
    };

    return JSON.stringify(dto);
  }
}
