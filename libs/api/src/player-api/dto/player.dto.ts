import { ApiProperty } from '@nestjs/swagger';

import { IPlayer } from '../interfaces';

export class PlayerDTO implements Omit<IPlayer, '_id'> {
  @ApiProperty({ example: '96AHDWAD7WDAW6DAH', required: true })
  readonly playerId: string;

  @ApiProperty({ example: 'Johan batman', required: true })
  readonly name: string;

  @ApiProperty({ example: 'Samsumg X', required: true })
  readonly device: string;

  @ApiProperty({ example: 'Tue, 31 May 2022 18:46:23 GMT', required: false })
  readonly createdDate: string;

  @ApiProperty({ example: 'Tue, 31 May 2022 18:46:23 GMT', required: false })
  readonly lastLogin: string;

  @ApiProperty({ example: '2.0.62', required: false })
  readonly gameVersion: string;

  @ApiProperty({ example: true, required: false, default: false })
  readonly isPremium: boolean;

  @ApiProperty({ example: 'AfA2345312', required: false })
  readonly discordId: string;

  constructor(player: IPlayer) {
    this.playerId = player.playerId;
    this.name = player.name;
    this.device = player.device;
    this.isPremium = player.isPremium;
    this.discordId = player.discordId;
    this.gameVersion = player.gameVersion;

    this.createdDate =
      player.createdDate && new Date(Number(player.createdDate)).toUTCString();
    this.lastLogin =
      player.lastLogin && new Date(Number(player.lastLogin)).toUTCString();
  }

  toString() {
    return JSON.stringify({
      ...new PlayerDTO(this),
    });
  }
}
