# Microservice deployer
### Nginx
First, you will need to install Nginx on all servers. You can install it with the following command:
Once installed, start the Nginx service and enable it to start at system reboot:

```
apt-get install nginx -y 

systemctl start nginx
systemctl enable nginx
```
### Docker
```
--- Managing Sarm
npm run init
npm run leave
npm run deploy-swarm
npm run destroy-swarm
npm run log

--- Scaling Services
docker service scale maze-api-swarm_api-gateway=5
docker service scale maze-api-swarm_notification=1
```