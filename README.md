<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

## Tower Defense Backend

```bash
# watch mode
$ npm run start:dev api-gateway
$ npm run start:dev notification

# package all services and build docker images
$ npm run package-all

# package individual service
$ npm run package:api-gateway
$ npm run package:notification
```
